// ------------------------------------------------------------
// Liberator Battle Rifle WITH NO BRIM
// ------------------------------------------------------------
const HDLD_ILIB="ilb";

class IronsLiberatorRifle:LiberatorRifle{
    default{
        //$Category "Weapons/Hideous Destructor"
		//$Title "Iron sights Liberator"
		//$Sprite "IBFLB0"

        inventory.pickupmessage "You got the battle rifle! No dot sight on this one.";
        hdweapon.refid HDLD_ILIB;
        tag "Iron sights Liberator battle rifle";
        inventory.icon "IBFLB0";
    }
    override void postbeginplay(){
        super.postbeginplay();
        weaponspecial=1337;
    }
    override string,double getpickupsprite(){
		string spr;

		// A: -g +m +a
		// B: +g +m +a
		// C: -g -m +a
		// D: +g -m +a
		if(weaponstatus[0]&LIBF_NOLAUNCHER){
			if(weaponstatus[LIBS_MAG]<0)spr="C";
			else spr="A";
		}else{
			if(weaponstatus[LIBS_MAG]<0)spr="D";
			else spr="B";
		}

		// E: -g +m -a
		// F: +g +m -a
		// G: -g -m -a
		// H: +g -m -a
		if(weaponstatus[0]&LIBF_NOAUTO)spr=string.format("%c",spr.byteat(0)+4);

		return ((weaponstatus[0]&LIBF_NOBULLPUP)?"IBLL":"IBFL")..spr.."0",1.;
	}
    override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		if(hdw.weaponstatus[0]&LIBF_GRENADEMODE)sb.drawgrenadeladder(hdw.airburst,bob);
		else{
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=Screen.GetClipRect();
		sb.SetClipRect(
			-16+bob.x,-4+bob.y,32,16,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*1.2;
		sb.drawimage(
			"frntsite",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.75,scale:(1.6,2)
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"backsite",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:(2,1.45)
		);
        if(scopeview){
				int scaledyoffset=60;
				int scaledwidth=72;
				double degree=hdw.weaponstatus[LIBS_ZOOM]*0.1;
				double deg=1/degree;
				int cx,cy,cw,ch;
				[cx,cy,cw,ch]=screen.GetClipRect();
				sb.SetClipRect(
					-36+bob.x,24+bob.y,scaledwidth,scaledwidth,
					sb.DI_SCREEN_CENTER
				);


				sb.fill(color(255,0,0,0),
					bob.x-36,scaledyoffset+bob.y-36,
					72,72,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
				);

				texman.setcameratotexture(hpc,"HDXCAM_LIB",degree);
				let cam  = texman.CheckForTexture("HDXCAM_LIB",TexMan.Type_Any);
				let reticle = texman.CheckForTexture(
					(hdw.weaponstatus[0] & LIBF_ALTRETICLE)? "reticle2" : "reticle1"
				,TexMan.Type_Any);

				vector2 frontoffs=(0,scaledyoffset)+bob*2;

				double camSize = texman.GetSize(cam);
				sb.DrawCircle(cam,frontoffs,.08825,usePixelRatio:true);

				let reticleScale = camSize / texman.GetSize(reticle);
				if(hdw.weaponstatus[0]&LIBF_FRONTRETICLE){
					sb.DrawCircle(reticle,frontoffs,393*reticleScale, bob*4, 1.6*deg);
				}else{
					sb.DrawCircle(reticle,(0,scaledyoffset)+bob,.403*reticleScale, uvScale: .52);
				}

				//see comments in zm66.zs
				//let hole = texman.CheckForTexture("scophole",TexMan.Type_Any);
				//let holeScale    = camSize / texman.GetSize(hole);
				//sb.DrawCircle(hole, (0, scaledyoffset) + bob, .403 * holeScale, bob * 5, uvScale: .95);


				screen.SetClipRect(cx,cy,cw,ch);

				sb.drawimage(
					"libscope",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
				);
				sb.drawstring(
					sb.mAmountFont,string.format("%.1f",degree),
					(6+bob.x,95+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
					Font.CR_BLACK
				);
				sb.drawstring(
					sb.mAmountFont,string.format("%i",hdw.weaponstatus[LIBS_DROPADJUST]),
					(6+bob.x,17+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
					Font.CR_BLACK
				);
			}
		}
	}
	states
	{
    select0:
		ILIB  A 0{
			A_Overlay(776,"brasstube");
			invoker.weaponstatus[0]&=~LIBF_GRENADEMODE;
		}goto select0big;
	deselect0:
		ILIB  A 0{
			while(invoker.weaponstatus[LIBS_BRASS]>0){
				double cosp=cos(pitch);
				actor brsss;
				[cosp,brsss]=A_SpawnItemEx("HDSpent7mm",
					cosp*12,0,height-8-sin(pitch)*12,
					cosp*3,0.2*randompick(-1,1),-sin(pitch)*3,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				brsss.vel+=vel;
				brsss.A_StartSound(brsss.bouncesound,volume:0.4);
				invoker.weaponstatus[LIBS_BRASS]--;
			}
		}goto deselect0big;
    ready:
		ILIB  A 1{
			if(pressingzoom()){
				if(player.cmd.buttons&BT_USE){
					A_ZoomAdjust(LIBS_DROPADJUST,0,600,BT_USE);
				}else if(invoker.weaponstatus[0]&LIBF_FRONTRETICLE)A_ZoomAdjust(LIBS_ZOOM,20,40);
				else A_ZoomAdjust(LIBS_ZOOM,6,70);
				A_WeaponReady(WRF_NONE);
			}else A_WeaponReady(WRF_ALL);
		}goto readyend;
    fire:
		ILIB  A 0{
			if(
				invoker.weaponstatus[0]&LIBF_NOLAUNCHER
				||!(invoker.weaponstatus[0]&LIBF_GRENADEMODE)
			){
				setweaponstate("firegun");
			}else setweaponstate("firegrenade");
		}
	hold:
		ILIB  A 1{
			if(
				invoker.weaponstatus[0]&LIBF_GRENADEMODE
				||!(invoker.weaponstatus[0]&LIBF_FULLAUTO)
				||(invoker.weaponstatus[0]&LIBF_NOAUTO)
				||invoker.weaponstatus[LIBS_CHAMBER]!=2
			)setweaponstate("nope");
		}goto shoot;

	firegun:
		ILIB  A 1{
			if(invoker.weaponstatus[0]&LIBF_NOBULLPUP)A_SetTics(0);
			else if(invoker.weaponstatus[0]&LIBF_FULLAUTO)A_SetTics(2);
		}
	shoot:
		ILIB  A 1{
			if(invoker.weaponstatus[LIBS_CHAMBER]==2)A_Gunflash();
			else setweaponstate("chamber_manual");
			A_WeaponReady(WRF_NONE);
		}
		ILIB  B 1 A_Chamber();
		ILIB  A 0 A_Refire();
		goto nope;
	flash:
		ILIF A 1 bright{
			A_Light1();
			A_StartSound("weapons/bigrifle",CHAN_WEAPON);

			HDBulletActor.FireBullet(self,
				invoker.weaponstatus[0]&LIBF_RECAST?"HDB_776r":"HDB_776",
				aimoffy:(-HDCONST_GRAVITY/1000.)*invoker.weaponstatus[LIBS_DROPADJUST]
			);
			if(invoker.weaponstatus[0]&LIBF_NOBULLPUP){
				HDFlashAlpha(16);
				A_ZoomRecoil(0.90);
				A_MuzzleClimb(
					0,0,
					-0.07,-0.14,
					-frandom(0.3,0.6),-frandom(1.,1.4),
					-frandom(0.2,0.4),-frandom(1.,1.4)
				);
			}else{
				HDFlashAlpha(32);
				A_ZoomRecoil(0.95);
				A_MuzzleClimb(
					0,0,
					-0.2,-0.4,
					-frandom(0.5,0.9),-frandom(1.7,2.1),
					-frandom(0.5,0.9),-frandom(1.7,2.1)
				);
			}

			invoker.weaponstatus[LIBS_CHAMBER]=1;
			invoker.weaponstatus[LIBS_HEAT]+=2;
			invoker.weaponstatus[0]&=~LIBF_RECAST;
			A_AlertMonsters();
		}
		goto lightdone;
	chamber_manual:
		ILIB  A 1 offset(-1,34){
			if(
				invoker.weaponstatus[LIBS_CHAMBER]==2
				||invoker.weaponstatus[LIBS_MAG]<1
			)setweaponstate("nope");
		}
		ILIB  B 1 offset(-2,36)A_Chamber();
		ILIB  B 1 offset(-2,38);
		ILIB  A 1 offset(-1,34);
		goto nope;
    unloadchamber:
		ILIB  B 1 offset(-1,34){
			if(
				invoker.weaponstatus[LIBS_CHAMBER]<1
			)setweaponstate("nope");
		}
		ILIB  B 1 offset(-2,36)A_Chamber(true);
		ILIB  B 1 offset(-2,38);
		ILIB  A 1 offset(-1,34);
		goto nope;

	loadchamber:
		ILIB  A 0 A_JumpIf(invoker.weaponstatus[LIBS_CHAMBER]>0,"nope");
		ILIB A 0 A_JumpIf(
			!countinv("SevenMilAmmo")
			&&!countinv("SevenMilAmmoRecast")
		,"nope");
		ILIB  A 1 offset(0,34) A_StartSound("weapons/pocket",9);
		ILIB  A 2 offset(2,36);
		ILIB  B 8 offset(5,40);
		ILIB  B 8 offset(7,44);
		ILIB  B 8 offset(6,43);
		ILIB  B 10 offset(4,39){
			class<inventory> rndtp="SevenMilAmmo";
			if(!countinv(rndtp))rndtp="SevenMilAmmoRecast";
			if(countinv(rndtp)){
				A_TakeInventory(rndtp,1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[LIBS_CHAMBER]=2;

				if(rndtp=="SevenMilAmmoRecast")invoker.weaponstatus[0]|=LIBF_RECAST;
				else invoker.weaponstatus[0]&=~LIBF_RECAST;
				A_StartSound("weapons/libchamber2",8);
				A_StartSound("weapons/libchamber2a",8,CHANF_OVERLAP,0.7);
			}else A_SetTics(4);
		}
		ILIB  B 7 offset(5,37);
		ILIB  B 1 offset(2,36);
		ILIB  A 1 offset(0,34);
		goto readyend;

	user4:
	unload:
		---- A 1 A_CheckChug(pressinguse()); //DO NOT set this frame to zero
		ILIB  A 0{
			invoker.weaponstatus[0]|=LIBF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[0]&LIBF_GRENADEMODE
			){
				return resolvestate("unloadgrenade");
			}else if(
				invoker.weaponstatus[LIBS_MAG]>=0  
			){
				return resolvestate("unmag");
			}else if(
				invoker.weaponstatus[LIBS_CHAMBER]>0  
			){
				return resolvestate("unloadchamber");
			}
			return resolvestate("nope");
		}
	reload:
		ILIB  A 0{
			int inmag=invoker.weaponstatus[LIBS_MAG];
			bool nomags=HDMagAmmo.NothingLoaded(self,"HD7mMag");
			bool haverounds=countinv("SevenMilAmmo")||countinv("SevenMilAmmoRecast");
			invoker.weaponstatus[0]&=~LIBF_JUSTUNLOAD;

			//no point reloading
			if(
						
				inmag>=30
				||(
					//no mags to load and can't directly load chamber
					nomags
					&&(
						!haverounds
						||inmag>=0
						||invoker.weaponstatus[LIBS_CHAMBER]>0
								 
					)
				)
			)return resolvestate("nope");

			//no mag, empty chamber, have loose rounds
			if(
				inmag<0
				&&invoker.weaponstatus[LIBS_CHAMBER]<1
				&&haverounds
				&&(
					pressinguse()
					||nomags
				)
			)return resolvestate("loadchamber");
			else if(
				invoker.weaponstatus[LIBS_MAG]>0  
			){
				//if full mag and unchambered, chamber
				if(
					invoker.weaponstatus[LIBS_MAG]>=30  
					&&invoker.weaponstatus[LIBS_CHAMBER]!=2
				){
					return resolvestate("chamber_manual");
				}				
			}
			return resolvestate("unmag");
		}

	unmag:
		ILIB  A 1 offset(0,34);
		ILIB  A 1 offset(2,36);
		ILIB  B 1 offset(4,40);
		ILIB  B 2 offset(8,42){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/rifleclick2",8);
		}
		ILIB  B 4 offset(14,46){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound ("weapons/rifleload",8,CHANF_OVERLAP);
		}
		ILIB  B 0{
			int magamt=invoker.weaponstatus[LIBS_MAG];
			if(magamt<0){setweaponstate("magout");return;}

			if(magamt>0){
				int fullets=clamp(30-invoker.weaponstatus[LIBS_RECASTS],0,magamt);
				magamt+=fullets*100;
			}

			invoker.weaponstatus[LIBS_MAG]=-1;
			invoker.weaponstatus[LIBS_RECASTS]=0;
			if(
				!PressingReload()
				&&!PressingUnload()
			){
				HDMagAmmo.SpawnMag(self,"HD7mMag",magamt);
				setweaponstate("magout");
			}else{
				HDMagAmmo.GiveMag(self,"HD7mMag",magamt);
				setweaponstate("pocketmag");
			}
		}
	pocketmag:
		ILIB  B 7 offset(12,52)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		ILIB  B 0 A_StartSound("weapons/pocket",9);
		ILIB  BB 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		ILIB  B 0{
		}goto magout;
	magout:
		ILIB  B 4{
			invoker.weaponstatus[LIBS_MAG]=-1;
			if(invoker.weaponstatus[0]&LIBF_JUSTUNLOAD)setweaponstate("reloaddone");
		}goto loadmag;


	loadmag:
		ILIB  B 0 A_StartSound("weapons/pocket",9);
		ILIB  BB 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.4),frandom(-0.2,0.8));
		ILIB  B 6 offset(12,52){
			let mmm=hdmagammo(findinventory("HD7mMag"));
			if(mmm){
				int minput=mmm.TakeMag(true);
				int rndcnt=minput%100;
				invoker.weaponstatus[LIBS_MAG]=rndcnt;
				invoker.weaponstatus[LIBS_RECASTS]=clamp(30-(minput/100),0,rndcnt);
				A_StartSound("weapons/rifleclick",8);
				A_StartSound("weapons/rifleload",8,CHANF_OVERLAP);
			}
		}
		ILIB  B 2 offset(8,46) A_StartSound("weapons/rifleclick2",8,CHANF_OVERLAP);
		goto reloaddone;

	reloaddone:
		ILIB  B 1 offset (4,40);
		ILIB  A 1 offset (2,34);
		goto chamber_manual;


	altfire:
		ILIB  A 1 offset(0,34){
			if(invoker.weaponstatus[0]&LIBF_NOLAUNCHER){
				invoker.weaponstatus[0]&=~(LIBF_GRENADEMODE|LIBF_GRENADELOADED);
				setweaponstate("nope");
			}else invoker.airburst=0;
		}
		ILIB  A 1 offset(2,36);
		ILIB  B 1 offset(4,40);
		ILIB  B 1 offset(2,36);
		ILIB  A 1 offset(0,34);
		ILIB  A 0{
			invoker.weaponstatus[0]^=LIBF_GRENADEMODE;
			A_SetHelpText();
			A_Refire();
		}goto ready;
	althold:
		ILIB  A 0;
		goto nope;


	firegrenade:
		ILIB  B 2{
			if(invoker.weaponstatus[0]&LIBF_GRENADELOADED){
				A_FireHDGL();
				invoker.weaponstatus[0]&=~LIBF_GRENADELOADED;
				if(invoker.weaponstatus[0]&LIBF_NOBULLPUP){
					A_ZoomRecoil(0.99);
					A_MuzzleClimb(
						0,0,
						-0.8,-2.,
						-0.4,-1.
					);
				}else{
					A_ZoomRecoil(0.95);
					A_MuzzleClimb(
						0,0,
						-1.2,-3.,
						-0.6,-1.4
					);
				}
			}else setweaponstate("nope");
		}
		ILIB  B 2;
		ILIB  A 0 A_Refire("nope");
		goto ready;
	altreload:
		ILIB  A 0{
			if(!(invoker.weaponstatus[0]&LIBF_NOLAUNCHER)){
				invoker.weaponstatus[0]&=~LIBF_JUSTUNLOAD;
				setweaponstate("unloadgrenade");
			}
		}goto nope;
	unloadgrenade:
		ILIB  A 1 offset(0,34){
			A_SetCrosshair(21);
			if(
				(
					//just unloading but no grenade
					invoker.weaponstatus[0]&LIBF_JUSTUNLOAD
					&&!(invoker.weaponstatus[0]&LIBF_GRENADELOADED)
				)||(
					//reloading but no ammo or already loaded
					!(invoker.weaponstatus[0]&LIBF_JUSTUNLOAD)
					&&(
						!countinv("HDRocketAmmo")
						||invoker.weaponstatus[0]&LIBF_GRENADELOADED
					)
				)
			){
				setweaponstate("nope");
			}
		}
		ILIB  A 1 offset(-5,40);
		ILIB  A 1 offset(-10,50);
		ILIB  A 1 offset(-15,56);
		ILIB  A 4 offset(-14,54){
			A_StartSound("weapons/pocket",9);
			A_StartSound("weapons/grenopen",8);
		}
		ILIB  A 3 offset(-16,56){
			if(invoker.weaponstatus[0]&LIBF_GRENADELOADED){
				if(
					(PressingReload()||PressingUnload())
					&&!A_JumpIfInventory("HDRocketAmmo",0,"null")
				){
					A_GiveInventory("HDRocketAmmo");
					A_StartSound("weapons/pocket",9);
					A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
					A_SetTics(6);
				}else A_SpawnItemEx("HDRocketAmmo",
					cos(pitch)*12,0,height-10-12*sin(pitch),
					vel.x,vel.y,vel.z,
					0,SXF_SETTARGET|SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				invoker.weaponstatus[0]&=~LIBF_GRENADELOADED;
			}
		}
		ILIB  A 0{
			if(invoker.weaponstatus[0]&LIBF_JUSTUNLOAD)setweaponstate("altreloaddone");
		}
		ILIB  AA 8 offset(-16,56)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		ILIB A 18 offset(-14,54){
			if(!countinv("HDRocketAmmo"))return;
			A_StartSound("weapons/grenopen",8);
			A_TakeInventory("HDRocketAmmo",1,TIF_NOTAKEINFINITE);
			invoker.weaponstatus[0]|=LIBF_GRENADELOADED;
		}
		ILIB B 4 offset(-12,50)A_StartSound("weapons/grenopen",8);
	altreloaddone:
		ILIB  A 1 offset(-15,56);
		ILIB  A 1 offset(-10,50);
		ILIB  A 1 offset(-5,40);
		ILIB  A 1 offset(0,34);
		goto nope;
    spawn:
		IBFL ABCDEFGH -1 nodelay{
			if(invoker.weaponstatus[0]&LIBF_NOBULLPUP){
				sprite=getspriteindex("IBLLA0");
			}
			// A: -g +m +a
			// B: +g +m +a
			// C: -g -m +a
			// D: +g -m +a
			if(invoker.weaponstatus[0]&LIBF_NOLAUNCHER){
				if(invoker.weaponstatus[LIBS_MAG]<0)frame=2;
				else frame=0;
			}else{
				if(invoker.weaponstatus[LIBS_MAG]<0)frame=3;
				else frame=1;
			}

			// E: -g +m -a
			// F: +g +m -a
			// G: -g -m -a
			// H: +g -m -a
			if(invoker.weaponstatus[0]&LIBF_NOAUTO)frame+=4;

			if(
				invoker.makinground
				&&invoker.brass>0
				&&invoker.powders>=3
			)setstatelabel("chug");
		}
		IBLL ABCDEFGH -1;
		stop;
    }
}
enum ironsliberatorstatus{
	LIBF_FULLAUTO=1,
	LIBF_JUSTUNLOAD=2,
	LIBF_GRENADELOADED=4,
	LIBF_NOLAUNCHER=8,
	LIBF_FRONTRETICLE=32,
	LIBF_ALTRETICLE=64,
	LIBF_GRENADEMODE=128,
	LIBF_UNLOADONLY=256,
	LIBF_NOBULLPUP=512,
	LIBF_NOAUTO=1024,
	LIBF_LEFTY=2048,
	LIBF_RECAST=4096,

	LIBS_FLAGS=0,
	LIBS_CHAMBER=1,
	LIBS_MAG=2, //-1 is ampty
	LIBS_ZOOM=3,
	LIBS_HEAT=4,
	LIBS_BRASS=5,
	LIBS_AIRBURST=6,
	LIBS_DROPADJUST=7,
	LIBS_RECASTS=9,
};

class IronsLiberatorRandom:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let lll=IronsLiberatorRifle(spawn("IronsLiberatorRifle",pos,ALLOW_REPLACE));
			if(!lll)return;
			HDF.TransferSpecials(self, lll);
			if(!random(0,2))lll.weaponstatus[0]|=LIBF_FRONTRETICLE;
			if(!random(0,2))lll.weaponstatus[0]|=LIBF_ALTRETICLE;
			if(!random(0,2))lll.weaponstatus[0]|=LIBF_NOLAUNCHER;
			if(!random(0,3))lll.weaponstatus[0]|=LIBF_NOBULLPUP;
			if(!random(0,5))lll.weaponstatus[0]|=LIBF_NOAUTO;

			if(lll.weaponstatus[0]&LIBF_NOLAUNCHER){
				spawn("HD7mMag",pos+(7,0,0),ALLOW_REPLACE);
				spawn("HD7mMag",pos+(5,0,0),ALLOW_REPLACE);
			}else{
				spawn("HDRocketAmmo",pos+(10,0,0),ALLOW_REPLACE);
				spawn("HDRocketAmmo",pos+(8,0,0),ALLOW_REPLACE);
				spawn("HD7mMag",pos+(5,0,0),ALLOW_REPLACE);
			}
		}stop;
	}
}
