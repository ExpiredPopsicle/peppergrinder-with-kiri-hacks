/*
    speed: 200-1000
    mass: in tenths of a gram
    pushfactor: 0.05-5.0 - imagine it being horizontal speed blowing in the wind
    accuracy: 0,200,200-700 - angle of outline from perpendicular, round deemed to be 20
    stamina: 900, 776, 426, you get the idea
    hardness: 1-5 - 1=pure lead, 5=steel (NOTE: this setting's bullets are (Teflon-coated) steel by default; will implement lead casts "later")
*/

const HDLD_50REV="ot5";

// ------------------------------------------------------------
// Otis-5 Revolver
// Barney's personal sidearm. Handcrafted by his good friend Otis Laurey some time after the invasion started.
// Otis named his own gun Barney, and Barney's gun Otis, so that they'd always be watching over each other.
// Tragically, just as Barney was arriving with a pizza so that they could watch their favorite movie, Predator, a horde of babuins broke through the window, setting themselves upon Otis.
// Dying in Barney's arms, his last words were "My mom's gonna worry when I don't make it home tonight!"
// Barney has never been the same, since. He set off with Otis's favorite and most prized possession, a Mario Funko Pop, and launched into a personal crusade against Hell.
// It can load two different types of rounds. One normal competition load, and a heavier, slower hand-loaded wildcat cartridge, made for hunting big, bipedal game.
// ------------------------------------------------------------
class HDOtisGun:HDHandgun{
	bool cylinderopen; //don't use weaponstatus since it shouldn't be saved anyway
	default{
		+hdweapon.fitsinbackpack
		+hdweapon.reverseguninertia
		scale 0.72;
		weapon.selectionorder 51;
		weapon.slotnumber 2;
		weapon.slotpriority 1.3;
		weapon.kickback 30;
		weapon.bobrangex 0.1;
		weapon.bobrangey 0.6;
		weapon.bobspeed 2.5;
		weapon.bobstyle "normal";
		obituary "%o will look nice above %k's fireplace... if they had one.";
		inventory.pickupmessage "You got the Otis-5 revolver!";
		tag "Otis-5 revolver";
		hdweapon.refid HDLD_50REV;
		hdweapon.barrelsize 24,0.3,0.5; //physically longer than auto but can shoot at contact
	}
	override double gunmass(){
		double blk=0;
		for(int i=OTIS_CYL1;i<=OTIS_CYL5;i++){
			int wi=weaponstatus[i];
			if(wi==OTIS_HEAVY)blk+=0.3;
			else if(wi==OTIS_LIGHT)blk+=0.3;
		}
		return blk+4.5;
	}
	override double weaponbulk(){
		double blk=0;
		for(int i=OTIS_CYL1;i<=OTIS_CYL5;i++){
			int wi=weaponstatus[i];
			if(wi==OTIS_HEAVY)blk+=ENC_50SW_LOADED;
			else if(wi==OTIS_LIGHT)blk+=ENC_50SW_LOADED;
		}
		return blk+52;
	}
	override string,double getpickupsprite(){
		return "OTIPA0",1.2;
	}

	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			sb.drawimage("SWRNB0",(-47,-10),sb.DI_SCREEN_CENTER_BOTTOM,scale:(2.0,2.0));
			sb.drawnum(hpl.countinv("HD500SWHeavyAmmo"),-44,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			sb.drawimage("SWRNA0",(-64,-10),sb.DI_SCREEN_CENTER_BOTTOM,scale:(2.0,2.0));
			sb.drawnum(hpl.countinv("HD500SWLightAmmo"),-60,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		int plf=hpl.player.getpsprite(PSP_WEAPON).frame;
		for(int i=OTIS_CYL1;i<=OTIS_CYL5;i++){
			double drawangle=i*(360./5.)-162;
			vector2 cylpos;
			if(plf==4){

				cylpos=(-30,-14);
			}else if(cylinderopen){
				cylpos=(-34,-12);
			}else{
				cylpos=(-22,-20);
			}
			double cdrngl=cos(drawangle);
			double sdrngl=sin(drawangle);
			if(
				!cylinderopen
				&&sb.hud_aspectscale.getbool()
			){
				cdrngl*=1.1;
				sdrngl*=(1./1.1);
			}
			vector2 drawpos=cylpos+(cdrngl,sdrngl)*5;
			sb.fill(
				hdw.weaponstatus[i]>0?
				color(255,240,230,40)
				:color(200,30,26,24),
				drawpos.x,
				drawpos.y,
				3,3,
				sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT
			);
		}
	}
	override string gethelptext(){
		if(cylinderopen)return
		WEPHELP_FIRE.." Close cylinder\n"
		..WEPHELP_ALTFIRE.." Cycle cylinder \(Hold "..WEPHELP_ZOOM.." to reverse\)\n"
		..WEPHELP_UNLOAD.." Hit extractor \(double-tap to dump live rounds\)\n"
		..WEPHELP_RELOAD.." Load round \(Hold "..WEPHELP_FIREMODE.." to use a standard load\)\n"
		;
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.." Pull back hammer\n"
		..WEPHELP_ALTRELOAD.."/"..WEPHELP_FIREMODE.."  Quick-Swap (if available)\n"
		..WEPHELP_UNLOAD.."/"..WEPHELP_RELOAD.." Open cylinder\n"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		vector2 scc;
		vector2 bobb=bob*1.3;

		sb.SetClipRect(
			-8+bob.x,-4+bob.y,16,10,
			sb.DI_SCREEN_CENTER
		);
		scc=(0.9,0.9);

		sb.drawimage(
			"otisftst",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9,scale:scc
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"otisbkst",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:scc
		);
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,10,20);
			double angchange=-10;
			if(angchange)owner.angle-=angchange;
			owner.A_DropInventory("HD500SWHeavyAmmo",amt);
			if(angchange){
				owner.angle+=angchange*2;
				owner.A_DropInventory("HD500SWLightAmmo",amt*2);
				owner.angle-=angchange;
			}
		}
	}
	
	
	override void ForceBasicAmmo(){
		owner.A_SetInventory("HD500SWHeavyAmmo",6);
	}
	override void initializewepstats(bool idfa){
		weaponstatus[OTIS_CYL1]=OTIS_HEAVY;
		weaponstatus[OTIS_CYL2]=OTIS_HEAVY;
		weaponstatus[OTIS_CYL3]=OTIS_HEAVY;
		weaponstatus[OTIS_CYL4]=OTIS_HEAVY;
		weaponstatus[OTIS_CYL5]=OTIS_HEAVY;
	}

	action bool HoldingRightHanded(){
		bool righthanded=invoker.wronghand;
		righthanded=
		(
			righthanded
			&&Wads.CheckNumForName("id",0)!=-1
		)||(
			!righthanded
			&&Wads.CheckNumForName("id",0)==-1
		);
		return righthanded;
	}
	action void A_CheckRevolverHand(){
		bool righthanded=HoldingRightHanded();
		if(righthanded)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("ROTGA0");
		else player.getpsprite(PSP_WEAPON).sprite=getspriteindex("OTSGA0");
	}
	action void A_RotateCylinder(bool clockwise=true){
		invoker.RotateCylinder(clockwise);
		A_StartSound("weapons/otiscyl",8);
	}
	void RotateCylinder(bool clockwise=true){
		if(clockwise){
			int cylbak=weaponstatus[OTIS_CYL1];
			weaponstatus[OTIS_CYL1]=weaponstatus[OTIS_CYL5];
			weaponstatus[OTIS_CYL5]=weaponstatus[OTIS_CYL4];
			weaponstatus[OTIS_CYL4]=weaponstatus[OTIS_CYL3];
			weaponstatus[OTIS_CYL3]=weaponstatus[OTIS_CYL2];
			weaponstatus[OTIS_CYL2]=cylbak;
		}else{
			int cylbak=weaponstatus[OTIS_CYL1];
			weaponstatus[OTIS_CYL1]=weaponstatus[OTIS_CYL2];
			weaponstatus[OTIS_CYL2]=weaponstatus[OTIS_CYL3];
			weaponstatus[OTIS_CYL3]=weaponstatus[OTIS_CYL4];
			weaponstatus[OTIS_CYL4]=weaponstatus[OTIS_CYL5];
			weaponstatus[OTIS_CYL5]=cylbak;
		}
	}
	action void A_LoadRound(){
		if(invoker.weaponstatus[OTIS_CYL1]>0)return;
		bool useninemil=(
			player.cmd.buttons&BT_FIREMODE
			||!countinv("HD500SWHeavyAmmo")
		);
		if(useninemil&&!countinv("HD500SWLightAmmo"))return;
		class<inventory>ammotype=useninemil?"HD500SWLightAmmo":"HD500SWHeavyAmmo";
		A_TakeInventory(ammotype,1,TIF_NOTAKEINFINITE);
		invoker.weaponstatus[OTIS_CYL1]=useninemil?OTIS_LIGHT:OTIS_HEAVY;
		A_StartSound("weapons/otisload",8,CHANF_OVERLAP);
	}
	action void A_OpenCylinder(){
		A_StartSound("weapons/otisopen",8);
		invoker.weaponstatus[0]&=~OTIF_COCKED;
		invoker.cylinderopen=true;
		A_SetHelpText();
	}
	action void A_CloseCylinder(){
		A_StartSound("weapons/otisclose",8);
		invoker.cylinderopen=false;
		A_SetHelpText();
	}
	action void A_HitExtractor(){
		double cosp=cos(pitch);
		for(int i=OTIS_CYL1;i<=OTIS_CYL5;i++){
			int thischamber=invoker.weaponstatus[i];
			if(thischamber<1)continue;
			if(
				thischamber==OTIS_LIGHTSPENT
				||thischamber==OTIS_HEAVYSPENT
			){
				actor aaa=spawn(
						"HDSpent500",
					(pos.xy,pos.z+height-10)
					+(cosp*cos(angle),cosp*sin(angle),sin(pitch))*7,
					ALLOW_REPLACE
				);
				aaa.vel=vel+(frandom(-1,1),frandom(-1,1),-1);
				invoker.weaponstatus[i]=0;
			}
		}
		A_StartSound("weapons/otiseject",8,CHANF_OVERLAP);
	}
	action void A_ExtractAll(){
		double cosp=cos(pitch);
		bool gotany=false;
		for(int i=OTIS_CYL1;i<=OTIS_CYL5;i++){
			int thischamber=invoker.weaponstatus[i];
			if(thischamber<1)continue;
			if(
				thischamber==OTIS_LIGHTSPENT
				||thischamber==OTIS_HEAVYSPENT
			){
				actor aaa=spawn("HDSpent500",
					(pos.xy,pos.z+height-14)
					+(cosp*cos(angle),cosp*sin(angle),sin(pitch)-2)*3,
					ALLOW_REPLACE
				);
				aaa.vel=vel+(frandom(-0.3,0.3),frandom(-0.3,0.3),-1);
				invoker.weaponstatus[i]=0;
			}else{
				//give or spawn either 9mm or 355
				class<inventory>ammotype=
					thischamber==OTIS_HEAVY?
					"HD500SWHeavyAmmo":"HD500SWLightAmmo";
				if(A_JumpIfInventory(ammotype,0,"null")){
					actor aaa=spawn(ammotype,
						(pos.xy,pos.z+height-14)
						+(cosp*cos(angle),cosp*sin(angle),sin(pitch)-2)*3,
						ALLOW_REPLACE
					);
					aaa.vel=vel+(frandom(-1,1),frandom(-1,1),-1);
				}else{
					A_GiveInventory(ammotype,1);
					gotany=true;
				}
				invoker.weaponstatus[i]=0;
			}
		}
		if(gotany)A_StartSound("weapons/pocket",9);
	}
	action void A_FireRevolver(){
		invoker.weaponstatus[0]&=~OTIF_COCKED;
		int cyl=invoker.weaponstatus[OTIS_CYL1];
		if(
			cyl!=OTIS_HEAVY
			&&cyl!=OTIS_LIGHT
		){
			A_StartSound("weapons/otisclick",8,CHANF_OVERLAP);
			return;
		}
		invoker.weaponstatus[OTIS_CYL1]--;
		bool masterball=cyl==OTIS_HEAVY;

		let bbb=HDBulletActor.FireBullet(self,masterball?"HDB_500LAD":"HDB_500SW",spread:1.,speedfactor:frandom(0.99,1.01));
		if(
			frandom(0,ceilingz-floorz)<bbb.speed*(masterball?0.4:0.3)
		)A_AlertMonsters(masterball?512:256);

		A_GunFlash();
		A_Light1();
		A_ZoomRecoil(0.995);
		HDFlashAlpha(masterball?72:64);
		A_StartSound("weapons/otisblast1",CHAN_WEAPON,CHANF_OVERLAP);
		if(masterball){	
			A_ZoomRecoil(0.85);
			A_MuzzleClimb(-frandom(2.,2.4),-frandom(4.6,5.));
			A_StartSound("weapons/otisblast1",CHAN_WEAPON,CHANF_OVERLAP,0.5);
			A_StartSound("weapons/otisblast2",CHAN_WEAPON,CHANF_OVERLAP,0.4);
		}else{
		A_ZoomRecoil(0.90);
			A_MuzzleClimb(-frandom(1.6,2.2),-frandom(4.2,4.6));
			A_StartSound("weapons/otisblast2",CHAN_WEAPON,CHANF_OVERLAP,0.3);
		}
	}
	int cooldown;
	action void A_ReadyOpen(){
		A_WeaponReady(WRF_NOFIRE|WRF_ALLOWUSER3);
		if(justpressed(BT_ALTATTACK))setweaponstate("open_rotatecylinder");
		else if(justpressed(BT_RELOAD)){
			if(
				(
					invoker.weaponstatus[OTIS_CYL1]>0
					&&invoker.weaponstatus[OTIS_CYL2]>0
					&&invoker.weaponstatus[OTIS_CYL3]>0
					&&invoker.weaponstatus[OTIS_CYL4]>0
					&&invoker.weaponstatus[OTIS_CYL5]>0
				)||(
					!countinv("HD500SWLightAmmo")
					&&!countinv("HD500SWHeavyAmmo")
				)
			)setweaponstate("open_closecylinder");
			else setweaponstate("open_loadround");
		}else if(justpressed(BT_ATTACK))setweaponstate("open_closecylinder");
		else if(justpressed(BT_UNLOAD)){
			if(!invoker.cooldown){
				setweaponstate("open_dumpcylinder");
				invoker.cooldown=6;
			}else{
				setweaponstate("open_dumpcylinder_all");
			}
		}
		if(invoker.cooldown>0)invoker.cooldown--;
	}
	action void A_RoundReady(int rndnm){
		int gunframe=-1;
		if(invoker.weaponstatus[rndnm]>0)gunframe=player.getpsprite(PSP_WEAPON).frame;
		let thissprite=player.getpsprite(OTIS_OVRCYL+rndnm);
		switch(gunframe){
		case 4: //E
			thissprite.frame=0;
			break;
		case 5: //F
			thissprite.frame=1;
			break;
		case 6: //G
			thissprite.frame=pressingzoom()?4:2;
			break;
		default:
			thissprite.sprite=getspriteindex("TNT1A0");
			thissprite.frame=0;
			return;break;
		}
	}
	action void A_CockHammer(bool yes=true){
		if(yes)invoker.weaponstatus[0]|=OTIF_COCKED;
		else invoker.weaponstatus[0]&=~OTIF_COCKED;
	}


/*
	A normal ready
	B ready cylinder midframe
	C hammer fully cocked (maybe renumber these lol)
	D recoil frame
	E cylinder swinging out - left hand passing to right
	F cylinder swung out - held in right hand, working chamber in middle
	G cylinder swung out midframe
*/
	states{
	spawn:
		OTIP A -1;
		stop;
	round1:OTC1 A 1 A_RoundReady(OTIS_CYL1);wait;
	round2:OTC2 A 1 A_RoundReady(OTIS_CYL2);wait;
	round3:OTC3 A 1 A_RoundReady(OTIS_CYL3);wait;
	round4:OTC4 A 1 A_RoundReady(OTIS_CYL4);wait;
	round5:OTC5 A 1 A_RoundReady(OTIS_CYL5);wait;
	select0:
		OTSG A 0{
			if(!countinv("NulledWeapon"))invoker.wronghand=false;
			A_TakeInventory("NulledWeapon");
			A_CheckRevolverHand();
			invoker.cylinderopen=false;
			invoker.weaponstatus[0]&=~OTIF_COCKED;

			//uncock all spare revolvers
			if(findinventory("SpareWeapons")){
				let spw=SpareWeapons(findinventory("SpareWeapons"));
				for(int i=0;i<spw.weapontype.size();i++){
					if(spw.weapontype[i]==invoker.getclassname()){
						string spw2=spw.weaponstatus[i];
						string spw1=spw2.left(spw2.indexof(","));
						spw2=spw2.mid(spw2.indexof(","));
						int stat0=spw1.toint();
						stat0&=~OTIF_COCKED;
						spw.weaponstatus[i]=stat0..spw2;
					}
				}
			}

			A_Overlay(OTIS_OVRCYL+OTIS_CYL1,"round1");
			A_Overlay(OTIS_OVRCYL+OTIS_CYL2,"round2");
			A_Overlay(OTIS_OVRCYL+OTIS_CYL3,"round3");
			A_Overlay(OTIS_OVRCYL+OTIS_CYL4,"round4");
			A_Overlay(OTIS_OVRCYL+OTIS_CYL5,"round5");
		}
		---- A 1 A_Raise();
		---- A 1 A_Raise(40);
		---- A 1 A_Raise(40);
		---- A 1 A_Raise(25);
		---- A 1 A_Raise(20);
		wait;
	deselect0:
		OTSG A 0 A_CheckRevolverHand();
		#### D 0 A_JumpIf(!invoker.cylinderopen,"deselect0a");
		OTSG F 1 A_CloseCylinder();
		OTSG E 1;
		OTSG A 0 A_CheckRevolverHand();
		goto deselect0a;
	deselect0a:
		#### AD 1 A_Lower();
		---- A 1 A_Lower(20);
		---- A 1 A_Lower(34);
		---- A 1 A_Lower(50);
		wait;
	ready:
		OTSG A 0 A_CheckRevolverHand();
		---- A 0 A_JumpIf(invoker.cylinderopen,"readyopen");
		#### C 0 A_JumpIf(invoker.weaponstatus[0]&OTIF_COCKED,2);
		#### A 0;
		---- A 1 A_WeaponReady(WRF_ALLOWRELOAD|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
		goto readyend;
	fire:
		#### A 0 A_JumpIf(invoker.weaponstatus[0]&OTIF_COCKED,"hammertime");
		#### B 2 offset(0,34);
		#### C 3 offset(0,36) A_RotateCylinder();
		#### A 0 offset(0,32);
	hammertime:
		#### A 0 A_ClearRefire();
		#### A 1 A_FireRevolver();
		goto nope;
	firerecoil:
		#### D 2;
		#### H 2;
		#### A 0;
		goto nope;
	flash:
		TNT1 A 0 A_JumpIf(player.getpsprite(PSP_WEAPON).sprite==getspriteindex("ROTGA0"),"flashright");
		OTSF A 1 bright;
		---- A 0 A_Light0();
		---- A 0 setweaponstate("firerecoil");
		stop;
		ROTG ABCD 0;
		stop;
	flashright:
		ROTF A 1 bright;
		---- A 0 A_Light0();
		---- A 0 setweaponstate("firerecoil");
		stop;
		ROTG ABCD 0;
		stop;
	altfire:
		---- A 0 A_JumpIf(invoker.weaponstatus[0]&OTIF_COCKED,"uncock");
		#### B 2 offset(0,34) A_ClearRefire();
		#### B 3 offset(0,36) A_RotateCylinder();
	cocked:
		#### C 0 A_CockHammer();
		---- A 0 A_JumpIf(pressingaltfire(),"nope");
		goto readyend;
	uncock:
		#### C 1 offset(0,38);
		#### B 1 offset(0,34);
		#### A 2 offset(0,36) A_StartSound("weapons/otiscyl",8,CHANF_OVERLAP);
		#### A 0 A_CockHammer(false);
		goto nope;
	reload:
	unload:
		#### C 0 A_JumpIf(!(invoker.weaponstatus[0]&OTIF_COCKED),3);
		#### B 2 offset(0,35)A_CockHammer(false);
		#### A 2 offset(0,33);
		#### A 1 A_JumpIf(player.getpsprite(PSP_WEAPON).sprite!=getspriteindex("ROTGA0"),"openslow");
		OTSG E 2 A_OpenCylinder();
		goto readyopen;
	openslow:
		OTSG A 1 offset(2,39);
		OTSG A 1 offset(4,50);
		OTSG A 1 offset(8,64);
		OTSG A 1 offset(10,86);
		OTSG A 1 offset(12,96);
		OTSG E 1 offset(-7,66);
		OTSG E 1 offset(-6,56);
		OTSG E 1 offset(-2,40);
		OTSG E 1 offset(0,32);
		OTSG E 1 A_OpenCylinder();
		goto readyopen;
	readyopen:
		OTSG F 1 A_ReadyOpen();
		goto readyend;
	open_rotatecylinder:
		OTSG G 3 A_RotateCylinder(pressingzoom());
		OTSG F 3 A_JumpIf(!pressingaltfire(),"readyopen");
		loop;
	open_loadround:
		OTSG F 4;
		OTSG F 2 A_LoadRound();
		goto open_rotatecylinder;
	open_closecylinder:
		OTSG E 2 A_JumpIf(pressingfire(),"open_fastclose");
		OTSG E 0 A_CloseCylinder();
		OTSG A 0 A_CheckRevolverHand();
		#### A 0 A_JumpIf(player.getpsprite(PSP_WEAPON).sprite==getspriteindex("ROTGA0"),"nope");
		OTSG E 1 offset(0,32);
		OTSG E 1 offset(-2,40);
		OTSG E 1 offset(-6,56);
		OTSG E 1 offset(-7,66);
		OTSG A 1 offset(12,96);
		OTSG A 1 offset(10,86);
		OTSG A 1 offset(8,64);
		OTSG A 1 offset(4,50);
		OTSG A 1 offset(2,39);
		goto nope;
	open_fastclose:
		OTSG E 2;
		OTSG A 0{
			A_CloseCylinder();
			invoker.wronghand=(Wads.CheckNumForName("id",0)!=-1);
			A_CheckRevolverHand();
		}goto nope;
	open_dumpcylinder:
		OTSG F 3 A_HitExtractor();
		goto readyopen;
	open_dumpcylinder_all:
		OTSG F 1 offset(0,34);
		OTSG F 1 offset(0,42);
		OTSG F 1 offset(0,54);
		OTSG F 1 offset(0,68);
		TNT1 A 6 A_ExtractAll();
		OTSG F 1 offset(0,68);
		OTSG F 1 offset(0,54);
		OTSG F 1 offset(0,42);
		OTSG F 1 offset(0,34);
		goto readyopen;

	user1:
	user2:
	swappistols:
		---- A 0 A_SwapHandguns();
		#### D 0 A_JumpIf(player.getpsprite(PSP_WEAPON).sprite==getspriteindex("ROTGA0"),"swappistols2");
	swappistols1:
		TNT1 A 0 A_Overlay(1025,"raiseright");
		TNT1 A 0 A_Overlay(1026,"lowerleft");
		TNT1 A 5;
		ROTG C 0 A_JumpIf(invoker.weaponstatus[0]&OTIF_COCKED,"nope");
		ROTG A 0;
		goto nope;
	swappistols2:
		TNT1 A 0 A_Overlay(1025,"raiseleft");
		TNT1 A 0 A_Overlay(1026,"lowerright");
		TNT1 A 5;
		OTSG C 0 A_JumpIf(invoker.weaponstatus[0]&OTIF_COCKED,"nope");
		OTSG A 0;
		goto nope;
	lowerleft:
		OTSG C 0 A_JumpIf(invoker.weaponstatus[0]&OTIF_COCKED,2);
		OTSG A 0;
		---- A 1 offset(-6,38);
		---- A 1 offset(-12,48);
		OTSG D 1 offset(-20,60);
		OTSG D 1 offset(-34,76);
		OTSG D 1 offset(-50,86);
		stop;
	lowerright:
		ROTG C 0 A_JumpIf(invoker.weaponstatus[0]&OTIF_COCKED,2);
		ROTG A 0;
		---- A 1 offset(6,38);
		---- A 1 offset(12,48);
		ROTG D 1 offset(20,60);
		ROTG D 1 offset(34,76);
		ROTG D 1 offset(50,86);
		stop;
	raiseleft:
		OTSG D 1 offset(-50,86);
		OTSG D 1 offset(-34,76);
		OTSG C 0 A_JumpIf(invoker.weaponstatus[0]&OTIF_COCKED,2);
		OTSG A 0;
		---- A 1 offset(-20,60);
		---- A 1 offset(-12,48);
		---- A 1 offset(-6,38);
		stop;
	raiseright:
		ROTG D 1 offset(50,86);
		ROTG D 1 offset(34,76);
		ROTG C 0 A_JumpIf(invoker.weaponstatus[0]&OTIF_COCKED,2);
		ROTG A 0;
		---- A 1 offset(20,60);
		---- A 1 offset(12,48);
		---- A 1 offset(6,38);
		stop;
	whyareyousmiling:
		#### D 1 offset(0,38);
		#### D 1 offset(0,48);
		#### D 1 offset(0,60);
		TNT1 A 7;
		OTSG A 0{
			invoker.wronghand=!invoker.wronghand;
			A_CheckRevolverHand();
		}
		#### D 1 offset(0,60);
		#### D 1 offset(0,48);
		#### D 1 offset(0,38);
		goto nope;
	}
}
enum OtisvolverStats{
	//chamber 1 is the shooty one
	OTIS_CYL1=1,
	OTIS_CYL2=2,
	OTIS_CYL3=3,
	OTIS_CYL4=4,
	OTIS_CYL5=5,
	OTIS_OVRCYL=355,

	//odd means spent
	OTIS_LIGHTSPENT=1,
	OTIS_LIGHT=2,
	OTIS_HEAVYSPENT=3,
	OTIS_HEAVY=4,

	OTIF_RIGHTHANDED=1,
	OTIF_COCKED=2,
}

class OtisSpawn:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let zzz=HDOtisGun(spawn("HDOtisGun",pos,ALLOW_REPLACE));
			HDF.TransferSpecials(self, zzz);
			spawn("HD500SWHeavyBoxPickup",pos+(3,0,0),ALLOW_REPLACE);
		}stop;
	}
}


