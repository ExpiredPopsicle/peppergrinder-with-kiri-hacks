Sleg's weapons (Code, most of the lore, texture work)
Otis-5: 
Sprites of Basilisk (from COD MW2022) ripped and sprited by a1337spy
Loaded bullet sprites by Doomnukem, edited by a1337spy
pickup sprite touched up by HyperUltra64
Light ammobox edited by me from a sprite by Pillowblaster from Trailblazer
Heavy ammobox edited by Prettyfist from my edit of the above
gunfoley sounds by Magmacow

TRO-G:
Sprites based on Chopblock's work, touched up by a1337spy, Hyperultra64, and Sledge.
Magazines derived from Freedoom's clip, edited by Sledge
Sight picture by a1337spy

9mmAR:
Sprites by HyperUltra64
Sounds from HL1, renovated by a1337spy

Guillotine:
sprites by potetobloke, based on chronoteeth's
Lore by ladyerisXII
sounds (by a1337spy) - cod mw2, fallout new vegas, counter strike global offensive, half life 2

scoped slayer
sprites by a1337spy and potetobloke

irons lib
sprites by a1337spy and potetobloke

Sawed Slayer
sprites from DRLA, by JoeyTD and Uboa (Used with permission!)

Vera (Light vulc)
sprites by Ultra64, sound from Halo I think probably

Bastard
Sprites by Combine_Kegan
Firing sound by 1337spy


prettyFist's Weapons (coding, edited, lore'd, and TEXTURE'd):
=============================================================
Box Cannon:
Weapon, pickup sprites, and hands by Skelegant (excellent and lovely!)
Firing sound from Wolfenstein 2009
Attachment Kit sprites by BloodyAcid

Ruby Maser:
Concept by Yholl (@yholl Hello!)
Sprites made by Sgt.Shivers
High power beam sound from BroForce
Low power beam sound from Star Wars Ep.1 Racer
Mode switch sounds from Deus Ex

Lotus Carbine:
Beautiful sprites edited by Wanzer, who I cannot thank enough for this, from
-CaptainJ's Thunderhawk
-High Noon Drifter by TerminusEst13
Scope, Supressor, & Barrel Extension sprites by 1337spy
Sounds mixed by 1337spy

Helzing Magnum:
Weapon & pickup sprites edited from Sgt.Shivers's Casull Pistol from DRLA
Holy shine sound from Castlevania: Dawn of Sorrow
Sounds by 1337spy

Greely-12:
Sprites edited from Captain Toenail's Centred Duke3d Shotgun
Additional edits & Ejection port by 1337spy
Sounds from CarnEvil (the game, not the man)

Oddball SMG:
Concept & Decorate version by Combine_Kegan
Sprites from Sonik.O fan's Doom SMG
Sprite Edits & Animation by Skelegant
Sounds & Sights by 1337spy
Porting to mattscript, bullet definitions, & balance tweaking by prettyFist
Brightmaps done by Kris

WF-60 Plasma Magnum & Rifle:
Sprites from Sonik.O Fan's Doom2016 Pistol & Rocket Launcher
Charging muzzle flashes from Captain Toenail's Ray Gun
Sounds mixed and put together by 1337spy
Charging sounds from Oni by Bungie

L&M BreakerTek P90:
Sounds by 1337spy
Red Crown P90 based off Devolver game Cult of the Lamb
photosource for sight by Breaker

Eric's weapons
=============================================================
Scoped Revolver:
Sonik O Fan's scope edited by Prettyfist

Mauler:
Sprites by Sgt. Shivers
Brightmaps by Muusi

Zorcher:
Sprites by Sgt. Shivers
Brightmaps by Muusi

ZM94:
Based on the ZM69 by Bogus
Sprites by Ultra64

spy's weapons
-------------------
yeyuze:
sgt mark iv - first person sprites, modified to remove the leaf sight
potetobloke - drop sprite, small edits by a1337spy
sounds - cod mw2, insurgency, half life 2, FEAR, firearms: source

bpx:
potetobloke - drop sprite, small edits by a1337spy
fps sprites - doom alpha by id software, reflex sight from sonik.o.fan, charging handle & muzzle flash by chopblock223, scope from captain j
sounds - cod black ops, cod modern warfare 2019, day of defeat, left 4 dead 2, insurgency

baileya:
potetobloke - drop sprite with edits by a1337spy, testing help
sonik.o.fan - auto-5 sprites with edits by a1337spy, muzzleflash by chopblock223, hand by marty kirra
sounds - cod black ops, killing floor, cod mw2019, l4d2, insurgency, half life 2
melodica for coding help and testing help

aurochs:
tg5 - sprites
potetobloke - sprites
sounds - cod mw2, half life 2, hotline miami 2, tfb tv, fallout new vegas
sledge - bullets
xcoldxfusionx - code snippets and idea for hammer fanning controls
testing - sledge, noah eadie, knox, eric


dmr:
chopblock223 - sprites
sounds - cod mw2019, firearms: source 2, insurgency, battlefield 4
testing - melodica, kris

coyote: 
idea - ultra54
sprites - tg5, ultra64 with edits by a1337spy
sounds - cod mw2019, counter strike source, hl2, killing floor, csgo
testing - prettyfist, knox, the5thbreaker, kris, ultra64, melodica

killer7:
sprites - tg5, potetobloke with edits by a1337spy
sounds - soldier of fortune (specifically, the desert eagle firing sound, which fucks massively), fallout new vegas, csgo, mw2019
testing - kris, ultra64, melodica

Melo weps
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Plasma Pistol:
-Plasma Pistol sprites edited from the Fusion Pistol from Marathon 2/Infinity and
 the Doom Pistol
-Plasma Pistol charge ball sprite by Yholl
-Sounds from Halo: CE
-plaspistol_overheat from Halo Reach
-plaspistol_overheat_small from Shadow Complex
-steam_loop22_02 from Timesplitters 2
-extra sounds from Marathon 2
-impact puff, and muzzle flashes edited from FreeDoom

Fulgur:
-Sprites by zrrion
-lasermgfire from the fallout 4 laser musket
-lasermgimpact from the slave zero plasma gun impact
-beam model by Pillowblaster

Yurei:
-Sprites by HorrorMovieRei, based on 3d model by Luchador over
 at Sketchfab (https://sketchfab.com/3d-models/spectre-m4-smg-0d2c4d977bbe4677bdd148ba626f1a61)
-Sights by Melodica, based off of the Hideous Destructor ones by Matt.
-Firing sound taken from Insurgency 35 Angry Bots mod, no idea where the original's from.

/////HUGE BIG OL THANKS/////
1337spy for providing a lot of audio design assistance and having an excellent ear for sounds.
Yholl, HyperUltra64, Eric, & Sgt.Shivers for providing feedback, sprites, sounds, and excellent advice.
Combine_Kegan for coming up with creative weapons and hucking them full force at us nearly feature complete.
Kiri for refactoring and cleaning up the event handler to be less smelly.
