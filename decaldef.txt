decal HelzingScorch1
{
	pic BFGLITE1
	x-scale 0.8
	y-scale 0.8
	shade "80 ff ff"
	fullbright
	randomflipx
	randomflipy
	animator BrontoAway
	lowerdecal Scorch
}

decal HelzingScorch2
{
	pic BFGLITE2
	x-scale 0.8
	y-scale 0.8
	shade "80 ff ff"
	fullbright
	randomflipx
	randomflipy
	animator BrontoAway
	lowerdecal Scorch
}

decalgroup HelzingScorch
{
	HelzingScorch1	1
	HelzingScorch2	1
}