# PEPPER GRINDER
# REQUIRES [BULLETLIB-RECASTED](https://github.com/HDest-Community/HDBulletLib-Recasted)!
### A WEAPONS MOD FOR [HIDEOUS DESTRUCTOR](https://codeberg.org/mc776/hideousdestructor)


## QUICKSTART

* Check the Peppergrinder sub-menu in the full options menu to enable or disable certain weapons/ammo types from spawning.
* Peppergrinder comes with loadout codes for Healing Potions, Blurspheres, and a variant of the elite soldier kit without a pistol.

## CONTENTS
-------------------
* **ITEMS**
* [AMMO CONVERSION DEVICE](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#ammo-conversion-device)
* [BINOCULARS](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#binoculars)
* [ZORCHER HANDHELD TELEPORTER](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#zorcher-handheld-teleporter)
-------------------
* **WEAPONS**
* [AUROCHS REVOLVER](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#aurochs-revolver)
* [BAILEYA REPEATER 5](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#baileya-repeater-5)
* [BASTARD RIFLE](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#bastard-rifle)
* [BOX CANNON PISTOL](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#box-cannon-pistol)
* [BPX PISTOL CALIBER CARBINE](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#bpx-pistol-caliber-carbine)
* [BREAKERTEK P90](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#breakertek-p90)
* [COYOTE AUTOMAG](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#coyote-automag)
* [FULGUR LASER MACHINEGUN](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#fulgur-laser-machine-gun)
* [GREELY-12 SUPER-SHORTY](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#greely-12-super-shorty)
* [HELZING MAGNUM PISTOL](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#helzing-magnum-pistol)
* [HLAR SMG](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#hlar-smg)
* [KILLER7](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#killer7)
* [LIBERATOR BATTLE RIFLE (IRONSIGHTS)](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#liberator-battle-rifle-ironsights)
* [LA PETITE GUILLOTINE](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#la-petite-gullotine)
* [LISA PLASMA BOLTER DMR](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#lisa-plasma-bolter-dmr)
* [7MM LOTUS CARBINE](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#7mm-lotus-carbine)
* [MAULER FRAG CANNON](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#mauler-frag-cannon)
* [ODDBALL PDW](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#oddball-pdw)
* [OTIS-5 REVOLVER](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#otis-5-revolver)
* [PG667 DESIGNATED MARKSMAN RIFLE](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#pg667-designated-marksman-rifle)
* [PLASMA PISTOL](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#plasma-pistol)
* [RUBY MICROWAVE LASER](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#ruby-microwave-laser)
* [SAWED OFF SHOTGUN](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#sawed-off-shotgun)
* [SCOPED REVOLVER](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#scoped-revolver)
* [SCOPED SLAYER](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#scoped-slayer)
* [TRO-G ASSAULT RIFLE](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#tro-g-assault-rifle)
* [VERA SQUAD AUTOMATIC WEAPON](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#vera-squad-automatic-weapon)
* [WISAEU PLASMA MAGNUM](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#wisaeu-plasma-magnum)
* [YEYUZE GRENADE LAUNCHER](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#yeyuze-grenade-launcher)
* [YUREI HIGH CAPACITY SMG](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#yurei-high-capacity-smg)
* [ZM94 "SABREWOLF" ANTI-MATERIEL RIFLE](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#zm94-sabrewolf-anti-materiel-rifle)
# ITEMS

## AMMO CONVERSION DEVICE
*Created by a1337spy, idea by Sledge and Eric*

GZDoom actor name: `TheBox`

Loadout code: `acb`

When the Occult Division's research on "Precious" artifacts hit a dead end, with all collected samples mysteriously vanishing, the team was tasked with "solving the ammo logistics problem" to justify their continued funding. Nobody expected actual results to come of asking some robe-wearing book nerds a strategic question, but after some experimentation with a 7mm reloader, this device emerged. When ammunition is placed inside, a proportionate amount of a different type of ammunition comes out. Whether this works through some scientific process or is simply a transaction with otherworldy beings (the official report makes reference to "brass goblins", but does not clarify whether these are real creatures or a metaphorical concept), nobody is willing to say, but the results are consistent and undeniably practical. Test units equipped with the box reported greatly reduced difficulty in maintaining supplies of exotic ammo types and remarked positively on being able to utilize supply drops for weapons they weren't carrying. The sole complaint came from a notably superstitious member of the unit, who claimed to have experienced "more frequent attack by hostile spirits", as well as noticing increased Frag concentration compared to previous ops.

Due to the complex rituals and manufacturing procedures needed to make the conversion processes work and not open a portal to Hell directly on top of you, these boxes are special issue only, and cannot be found in the world. 

Instructions: 

To use the Box, open it from your inventory.

Hold Firemode and Fire to scroll the list of ammo types up, hold Firemode and Altfire to scroll it down.

Hold Alt Reload and Fire to increase the amount of rounds created, hold Alt Reload and Altfire to decrease the amount of rounds created.

Hold Fire to activate the ammo conversion device and throw it on the ground. You'll know when it's done.

Hold Zoom and press Altfire to toggle magazine conversion mode.

Protip: Even if you are not using any special ammo types, the box is still useful for converting those special types back into standard ones. 

## BINOCULARS 
*Created by prettyFist*

GZDoom actor name: `HDBinoculars`

Loadout code: `bnc`
* lowres - 0/1, Lower resolution for weaker hardware


A pair of Binoculars. Use these to see further, or to substitute for a scope.

Instructions:

Equip the binoculars from your inventory.

Hold Zoom to look down the binoculars.

Hold Zoom and Firemode to adjust the zoom on the binoculars.

##  ZORCHER HANDHELD TELEPORTER
*Created by Eric*

GZDoom actor name: `HDZorcher`

GZDoom ammo actor names: `HDBattery`

Ammo loadout code: `bat`

Loadout code: `zrc`

A curious handmade tool recovered from the corpse of a cereal miner found in the caverns of Bazoik. It can teleport anything, anywhere, an impressive technical feat, but whoever built it was either strapped for time or simply lacking common sense.

With no means of configuring the destination and only the barest minimum of safety features, it's not about to revolutionize transportation. While it can teleport living beings without any apparent bodily harm, there's no guarantee that they won't find themselves falling out of the sky, stranded in a wasteland, or smack in the middle of an enemy ambush. With that in mind, it's almost a weapon. Our enemies may not be so susceptible to environmental hazards, but displacing high-value targets in their formations could prove effective.

Instructions: 

Select the Zorcher from your inventory.

Press Fire to teleport the target in front of you. 

Reload to change batteries.

Unload to remove the battery.

Protip: The teleportation makes no sanity checks beyond ensuring that the destination is within valid level geometry. Teleported things may end up in unreachable or inescapable locations, or stuck too close to a wall to be able to move away from it. 


# WEAPONS

## AUROCHS REVOLVER
*Created by a1337spy*

GZDoom actor name: `HDAurochs`

GZDoom ammo actor name: `HDAurochsAmmo`

Ammo loadout codes: `4fr`, `66b`

Default weapon slot: 2

Loadout code: `aur`
* shotshells - 0/1, Starts with .066 bore shotshells loaded
* clockwise - 0/1, Makes the revolver rotate clockwise when cocking
* alt - 0/1, Gives the revolver an alternate color scheme and finish, no advantages over the stock revolver


A single action revolver produced by the VPF Arms Company after their surprising success with the YEYUZE grenade launcher, this weapon was a personal request from the mysterious owner of the company, and is perfect for cowboy shooters. Chambered in .451 Frei, this revolver can also load and fire .066 bore shotshells at reduced velocity.

Instructions:

Pull the hammer back by pressing altfire, then press fire to shoot. The hammer must be manually pulled back before every shot. Good luck.

With the hammer down, hold fire and press altfire to fan the hammer.

You can carry a second revolver in your off-hand. Press Alt Reload to swap between the revolver in your off hand and your main hand.

To Reload:
Press Reload/Unload to open the loading gate, the weapon will open to the first round clockwise.

Press Unload to hit the extractor, rapidly double tap to extract live rounds.

Press Reload to load a .451 Frei round, hold Firemode and press Reload to load a .066 bore shotshell.

Press Altfire to cycle the cylinder counter-clockwise, hold Zoom and Altfire to cycle the cylinder clockwise.

Press Fire to close the loading gate.

Protip: The Aurochs normally rotates counter clockwise. This means in a panic you can quickly load a round in, pull the hammer back, then fire.

## BAILEYA REPEATER 5
*Created by a1337spy*

GZDoom actor name: `HDAuto5`

GZDoom ammo actor names: `HDSlugAmmo`

Ammo loadout codes: `slu`

Default weapon slot: 3

Loadout code: `br5`
* scope - 0/1, Adds a hunting scope to the weapon, removes iron sights
* bulletdrop - 0-1200, amount of compensation for bullet drop
* zoom - 6-70, 10x the resulting FOV in degrees


A semi-automatic shotgun manufactured by Mrowning & Sons LTD, a rifled barrel is installed on the weapon to increase the range and power of commercially available slugs. However, due to the weapon's extremely sturdy construction, it is able to fire the normally destructive Ümlaut slugs used by the Greely 12. The combination of the rifled barrel and extremely overtuned projectile plus powder load causes the slugs to hit like a ton of bricks. 

The weapon also includes a slug caddy in front of the bolt for easy chamber loading.

Instructions:

Press Fire to shoot the gun. 

Press Altfire to pull the bolt back, in the event of a underloaded round not cycling the bolt. 

Reloading is identical to the hunter shotgun, same with unloading.

To chamber load from the slug caddy, hold altfire on an empty chamber, then press Reload. Press Alt Reload to load a slug from your inventory.

Press Alt Reload to load the slug caddy. Press Use + Unload to unload it. 

As with the other scoped weapons, hold Zoom and Firemode to adjust scope zoom, or Use to adjust bullet drop.

## BASTARD RIFLE
*Created by Sledge, idea by Combine_Kegan*

GZDoom actor name: `HDBastardRifle`

Ammo loadout code: `4mm`

Default weapon slot: 4

Loadout code: `bst`
* firemode - 0/1, 0 Semiauto, 1 Burst
* glready - 0/1, Hairtrigger GL Loaded

While it has gone on record that Lt. Krognar Lodyte designed the TRO-G, after his death and later resurrection as a cyborg, Lt. Krognar found the small magazine capacity of the TRO-G had gotten his new body destroyed on several instances, mainly when dealing with larger demons such as Hell Knights in close quarters.

After enough of these incidents of being scrapped and repaired, several eyewitnesses reported the Lieutenant had grabbed one of the mechanics, stating "You're going to help me make a gun better suited for killing those goat bastards. I'm not gonna get into anymore debt with these assholes than I already have."

After a few weeks with a leftover TRO-G and much table flipping, the Bastard had been brought into this world. A far less elegant weapon, the Bastard's most notable feature is the built in feeder mechanism, doing away with magazines, the rifle is fed stray 4.26 rounds and is capable of holding roughly 100 rounds at any given time. Alongside this, the Bastard has a modified firing mechanism, sacrificing the full auto firemode in favor of the hyperburst similar to the now mainstream ZM66, but without the risk of a cookoff thanks to the vented barrel and the ammo separation of the hopper system.

It should be noted however that due to this bizarre configuration  and using fragile 4mil cartridges, there is always a small chance of the weapon breaking ammunition while being fired. The Bastard is durable enough to handle breakages without worry of jams or cookoffs, but ammo waste is a common side effect of the weapon.

Like the TRO-G, the Bastard also features an integrated grenade launcher with the same trigger group. A deliberate choice from  Krognar as he has grown accustomed to the hair trigger grenade launcher configuration.

Naturally this weapon was not licensed or endorsed by Volt whatsoever, making the rifle and the story behind it another strange anecdote in Volt's history, though Volt themselves have refused to provide any further commentary.

Instructions:

Press Fire to shoot.

Press Altfire to fire a grenade. Be aware that the launcher is on a hair trigger.

Press Reload to start loading rounds into the hopper.

Press Alt Reload to insert a rocket grenade into the launcher.

Hold Unload to unload the hopper.

Press Firemode and Unload to unload the grenade launcher.

## BOX CANNON PISTOL
*Created by prettyFist*

GZDoom actor name: `HDBoxCannon`

GZDoom ammo actor names: `HD9mClip`, `HDPistolAmmo`

Ammo loadout codes: `910`, `9mm`

Default weapon slot: 2

Loadout code: `bc9`
* stock - 0/1, Adds the Broomhandle Stock for additional stability and reduction of recoil
* xmag - 0/1, Enhances the internal magazine to the much larger 20 round model. Incompatible with Drum Magazine
* drum - 0/1, Replaces the internal magazine with a bulky 40 round minidrum. Extra dakka, extra weight. Incompatible with Extended Magazine
* barrel - 0/1, Refactors the barrel to a rifle barrel with front grip. Increases stability further along with accuracy and velocity. Incompatible with the Mini-Tsuchinoko Silencer
* silencer - 0/1, Attaches a Mini-Tsuchinoko Silencer to the barrel. Incompatible with the rifle barrel

Loadout shortcuts:
* leon - Stock and Extended Magazine
* carbine - Stock and Rifle Barrel
* chicago - Drum Mag and Rifle Barrel
* assaulter - Drum Mag, Rifle Barrel, and Stock


Initially produced by Lucarde & Morrison for a private security company's arms contract for a lightweight but accurate pistol, the Box Cannon C24 Pistol Carbine became the reliable sidearm of Belmondo special operatives during the war against the Conluz cultists, former followers of apocalyptic prophet Jones Graham.

It was when Conluz began summoning large swathes of demons in a bid for power that the Box Cannon found it's most use as it was distributed into the hands of the elite W.I.N.G. (Wallachian Infiltration Night Guards) Unit, where it was most popular with certain agents who claim it could "fire bullets harder" and "surpass any other pistol choice" despite being confronted with logical evidence to the contrary. The commonly paired magazine extension, lightweight dual purpose holster/stock, and stripper clips made it a reliable and sturdy weapon that is easy to hastily load from salvaged rounds deep behind enemy lines.

Despite allegations of supplying both sides with alarming amounts of 9mm munitions for profit, the Green Hawk Ammunition Company only commented "We never distributed any to that region, we're fairly certain it's just one scary kid wearing bellbottom jeans. It's fine, don't worry about it."

Instructions:

Press Fire to fire the weapon.

Press Firemode to switch to a three round burst. 

Press Reload to load from a 10 round 9mm stripper clip, press Firemode and Reload to load from loose 9mm in your inventory.

Press Alt Reload while carrying two box cannons to swap between them. Stocked configurations cannot be quick swapped away from.

Protip: Because the stock doubles as a holster, it adds minimal bulk to an otherwise standard configuration. This benefit does not apply to any configuration with a modified barrel or magazine, as the gun won't fit inside the holster anymore. 

## BPX PISTOL CALIBER CARBINE
*Created by a1337spy*

GZDoom actor name: `HDBPX`

GZDoom ammo actor names: `HD9mMag15`, `HDPistolAmmo`

Ammo loadout codes: `915`, `9mm`

Default weapon slot: 2

Loadout code: `bpx`
* reflex - 0/1, Mounts a LIG Mauer Rumia4 reflex sight onto the weapon. Incompatible with scope
* scope - 0/1, Mounts a LIG Mauer Dragon7 scope. Incompatible with reflex sight
* bulletdrop - 0-1200, amount of compensation for bullet drop
* zoom - 6-70, 10x the resulting FOV in degrees


The LIG Mauer BPX PCC was produced for the target shooting market, where low recoil and fast moving projectiles necessitated its chambering in 9x22mm. The weapon has seen recent use as a lighter alternative to some larger caliber rifles for longer ranges. 

Most BPX Carbines come out of the box with a magazine pouch affixed to the stock.

Instructions:

Controls are similar to SMG. 

If a scope is mounted, then hold Zoom and Firemode to adjust scope zoom, or Use to adjust bullet drop.

Press Alt Reload to reload a magazine from your inventory instead of the pouch.

Press Firemode and Reload to insert magazines into the magazine pouch.

Press Firemode and Unload to remove and drop magazines from the pouch.

## BREAKERTEK P90
*Created by prettyFist & The5thBreaker*

GZDoom actor name: `HDBreakerp90`

GZDoom ammo actor name: `HDNDMBigMag`, `HDNDMLoose`

Ammo loadout codes: `n50`, `ndm`

Default weapon slot: 2

Loadout code: `p90`
* firemode - 0/1, 0 Semiauto, 1 Auto
* civilian - 0/1, Semi-Only Civilian Model
* grip - 0/1, Foregrip (Civilian Only)

Drafted by L&M for a contract to support the use of the 6x30mm Federal Negotiation round, the BreakerTek P90 quickly reliable firearm that excelled in every way that flew off shelves due to it's strength. Unfortunately, the branch of the LCA government had its funding cut to support VOLT's acquisition of streamlined caseless cartridge production, causing the reliable 6mm round to be discontinued immediately.

Luckily for owners of the BreakerTek P90, the notchings on the space-age plastic BreakerTek mag meant to hold 6mm loosely line up with the unique shape of the largely distributed Nail Driver Magnum loads found commonly in backalley firearms deals and nunneries. Since neither the magazine nor the gun was designed for the slightly smaller round, a colourful variety of issues pop up when using NDM as the primary ammunition. The shorter cartridge means the round doesn't seat properly when loading into the chamber. As the internal mechanisms heat up as the gun fires, the parts begin warping slightly farther apart, greatly increasing the chance of the round jamming on entry. Faliure to feed causes the round to lodge in the feeding mechanism of the magazine, requiring removal of the magazine and breaking the catch, flinging the remainder of the magazine everywhere.

Instructions:

Most controls are identical to the default SMG.

**RELOAD DOES NOT IMPLY UNLOAD.**

Press Unload to remove the magazine.

Press Reload to insert a new magazine.

When jammed, press unload to remove the magazine from the weapon.

## COYOTE AUTOMAG
*Created by a1337spy, idea by Ultra64*

GZDoom actor name: `HDCoyote`

GZDoom ammo actor name: `HDCoyoteMag`

Ammo loadout codes: `407`, `451`

Default weapon slot: 2

Loadout code: `coy`
* gl - 0/1, Replace 12ga underbarrel with 20mm grenade launcher

The Wild Coyote .451 AUTOMAG was originally built to be a hunting handgun, able to handle high pressure cartridges due to its heavy frame. While hunting deer can be a very enjoyable activity, security specialists across the galaxy find themselves hunting much more aggressive targets than something from the peaceful forests of Earth.

In recent years, it has become very popular for armed forces, both governmental and private, to carry magnum chambered handguns. Some say it was due to the heroic actions of a colonial security cyborg, while others say it was because of "some science lab mall cop with dead eyes" who single handedly slaughtered hundreds of monsters. Regardless of what reason made them admired again, a massive resurgence of heavy pistols have once again found a large market to satisfy.

The Coyote is no exception to this phenomenon. This heavyweight handgun fires powerful .451 cartridges, and also has an emergency 12 gauge shotgun under the barrel. It should be noted that the shotgun element of the weapon is intended for point blank use. The weapon itself is also too heavy to be dual wielded: you'll need both of those hands to fire this steadily. If your other hand could be free, you'd probably be using it to plug one of your ears. Variants of this weapon with a 20mm grenade launcher can also be found. While a far cry from standard 40mm munitions, the grenades still pack a respectable punch on direct hits, and feature a shorter arming distance with less risk to the user.

Instructions:

Most controls are identical to the default Pistol.

**There is no off hand swapping with the Coyote.**

Press Altfire to fire the underbarrel shotgun/launcher.

Press Alt Reload to reload the underbarrel shotgun/launcher.

Press Firemode + Unload to unload the underbarrel shotgun/launcher.

Protip: Use the underbarrel immediately after firing one round of .451 to stun or kill Hell Knights, Arachnotrons, and Mancubi quickly and efficiently.

## FULGUR LASER MACHINE GUN
*Created by Melodica*

GZDoom actor name: `HDLaserMachinegun`

GZDoom ammo actor names: `HDBattery`

Ammo loadout code: `bat`

Default weapon slot: 6

Loadout code: `lsm`
* alt - 0/1, whether to start in manual focus mode


With the TB's unexpected popularity, research into more energy based infantry weapons led to the 'Fulgur' Laser Machinegun, taking existing laser point defense technology and slapping it into a man portable package based on the frame of the Thunder Buster, at the cost of the big scary expensive computers required to handle quickly focusing the laser accurately against a target. The weapon can also load and draw power from two cell batteries.

Instructions:

Press Fire to fire the weapon.

Press Reload to swap primary cell batteries.

Press Alt Reload to swap secondary cell batteries.

Press Unload to remove the primary cell battery. Press Use and Unload to remove the secondary cell battery.

Press Altfire to swap to manual focus mode.

Press Firemode and move the mouse to adjust the focus distance, same as adjusting airburst.

Protip: When in automatic mode, the Fulgur will take a split second to calculate the beam distance. Manual mode does not have this delay. Mind the beam's effects in close quarters.

## GREELY-12 SUPER-SHORTY
*Created by prettyFist*

GZDoom actor name: `HDGreely`

GZDoom ammo actor names: `HDShellAmmo`, `HDSlugAmmo`

Ammo loadout codes: `shl`, `slu`

Default weapon slot: 3

Loadout code: `gre`
* shell - 0/1, Start with shells loaded


Custom designed by a zerk junkie who preferred to hunt game by sprinting it down, the Greely is a small but sturdy pump shotgun designed under the expectation that it will be manhandled, thrown, beaten, smacked, punched, hucked, and punted in every way, shape, and form. Reinforcing the internals to prevent destruction or loosening of any mechanics upon the weapon being thrown at fastball speeds into a steel wall removes any ability to utilize delicate mechanisms as those needed to operate automatic shell loading, forcing the user to manually rack the pump themself. 

An unforeseen benefit of the heavily reinforced construction is the dense alloy barrel and unshakeable internals allow for slugs of unprecedented size that would obliterate a standard Hunter to be loaded. Unfortunately, the beastly custom Ümlaut 12ga Slugs come at the cost of sacrificing any possibility of a choke, as they would tear any threading or cinching directly out of the barrel.

Instructions:

Similar controls to the Hunter shotgun.

Press Alt Reload to load slugs.

Hold Altfire and press Alt Reload to insert a slug into the chamber.

Protip: You can perform a Slug/Shell changeover on a loaded chamber. To do this, perform a slug/shell chamberload as normal on a loaded chamber.

## HELZING MAGNUM PISTOL
*Created by prettyFist*

GZDoom actor name: `HDHelzing`

GZDoom ammo actor names: `HD355mag`, `HDRevolverAmmo`

Ammo loadout codes: `312`, `355`

Default weapon slot: 2

Loadout code: `hlz`
* justone - 0/1, Start with one sip of healing magic in the weapon
* holy 0/1, Start with a full healing potion in the weapon


A strange pistol chambered in .355 Masterball with a spray nozzle integrated into the body. While initially made by an insane person predicting the coming end of days, the bottle attachment found use with coating rounds with holy water, increasing their effectiveness exponentially. A multi-action trigger pull sprays the round right before the hammer is pulled. Otherwise still completely functional as a regular pistol chambered in .355 Masterball.

Instructions:

Controls are similar to the Pistol.

Press Firemode to open the valve.

While the valve is open:

Press Reload to swap healing potions.

Press Unload to remove the loaded healing potion.

When a potion is loaded, press Fire to shoot a consecrated round.

Press Firemode again to close the valve.

Protip: One consecrated round will instantly destroy any enemy's shield, no matter how strong.

## HLAR SMG
*Created by Sledge*

GZDoom actor name: `HDHLAR`

GZDoom ammo actor names: `HD9mMag30`, `HDPistolAmmo`

Ammo loadout codes: `930`, `9mm`

Default weapon slot: 2

Loadout code: `9ar`
* firemode - 0-2, 0 Semiauto, 1 Burst, 2 Auto
* glready - 0/1, Hairtrigger GL Loaded
* revised - 0/1, Revised Trigger Group


The HLAR (Hecate Lazulum Assault-Recon, a peculiar name only later discovered under a coffee stain on a forgotten document in a lawyer's desk,) underwent limited mass-production, used as the personal and primary armament of the suspiciously well-funded (and notoriously under-prepared) Demonic Environment Combat Unit. One stitch, of course, being that the creation of this group predated any widespread awareness of "mishaps" with frag energy, let alone any proper invasions. This fact, and rather alarming distribution of taxpayer funds away from typical benefactories of armed forces (Volt, AGM, UAC, et al.) resulted in a typhoon of litigation directed against the DECU and their mysterious backers. 

Despite this, eyewitnesses confirm that some local dispatches of the unit proved more effective than expected, particularly where frag shields were involved, but this fact was buried and silenced where necessary by Volt's army of lawyers, judges, and general places of authority held by shareholders. What few survivors there were can confirm the veracity and indisputably trigger-happy efficacy of the users of this gun, but on average, their training could not match their enthusiasm. 

Certain early models of this gun had a unique trigger group that included the GL's with the SMG, but after a few mishaps during training operations in nearby forests (then dismissed at the time as weather balloons exploding spontaneously), it was made to adhere to a more sensible standard of two different trigger groups. The project was halted (and any further designs canned) by furious and persistent litigation upon the parent company, Ebon Butte, by a rival company, Interstice Science, a subsidiary of Volt, which culminated in a massive public discrediting and smearing of any and all projects or activities performed by either organization. This task was made more simple by the Sonority Deluge disaster that left a country-sized crater in the middle of the US/Canadian/Liechtensteiner bloc. (Volt assures that this had nothing to do with any concurrent experiments by Interstice.)

Instructions: 

Controls are identical to the normal SMG.

Press Altfire to fire a rocket grenade. Be extremely careful, as the launcher is on a hair trigger.

Press Alt Reload to insert a rocket grenade into the launcher. 

Press Firemode and Unload to remove a loaded rocket grenade.

Protip: Naturally spawned HLARs can come in either trigger group variant. Make sure to check the controls before making assumptions about what will happen when you press Altfire. 

## KILLER7
*Created by a1337spy*

GZDoom actor name: `HDKiller7`

GZDoom ammo actor names: `HDKiller7Mag`, `HD500SWLightAmmo`

Ammo loadout codes: `507`, `5sw`

Default weapon slot: 2

Loadout code: `kl7`


An absolute monster of a hand cannon, the VPF Killer7 came about as part of a bet between the owner of the company and an unnamed accountant to see "how big you can make these damned things." The only logical answer was to chamber the weapon in .500 Magnum and put a low power scope on it. 

The Killer7 became known due to its usage by a rogue UAC Combat Android's rampage against several members of the UAC Board of Artificial Life, which resulted in the weapon exploding in an ironic surge of popularity amongst big game hunters and fans of magnum caliber handguns. 

VPF has regularly attempted to push the weapon off into obscurity, with each try failing in some way and causing the weapon to come back to the forefront.

Instructions:

All controls are identical to the default Pistol.

Press Zoom to use the low power scope, take note that the magnification cannot be adjusted.

Protip: Similar to the [Scoped Revolver](https://gitlab.com/hdiscord-saltmines/hd-peppergrinder/-/blob/master/MANUAL.md#scoped-revolver), the Killer7 does not unbrace when firing.

## LIBERATOR BATTLE RIFLE (IRONSIGHTS)
*Created by Sledge*

GZDoom actor name: `IronsLiberatorRifle`

GZDoom ammo actor names: `HD7mMag`, `SevenMilAmmo`

Ammo loadout codes: `730`, `7mm`

Default weapon slot: 6

Loadout code: `ilb` 
* All sub-loadout codes are identical to the normal Liberator


A Liberator Battle Rifle with the red dot sight removed. For freaks.

Instructions:

Controls are identical to the normal Liberator.

## LA PETITE GULLOTINE
*Created by Sledge, idea by LadyErisXII*

GZDoom actor name: `HDGuillotine`

GZDoom ammo actor names: `HDNDMMag`, `HDNDMLoose`

Ammo loadout codes: `n15`, `ndm`

Default weapon slot: 2

Loadout code: `lpg`


A unique piece, designed by the talented Belgian gunsmith Emeline Leone. Built off the Mouletta 99FX platform it chambers the powerful 9mm Nail Driver Maximum.

Originally created for competition shooting, the 9mm NDM cartridge was quickly adapted for use as a self defense cartridge against violent gangs of armored Mimes with the addition of a hardened tungsten penetrator. Before long it found itself being used in the battle against the tyrant's forces with great effect. It is unknown where this pistol came from, or where it's creator is though upon closer inspection you see it is worn, with dried blood and scratch marks on the butt of the pistol. Though some rumors claim she has retired to the Belgian countryside.

The 9mm NDM traces it's roots back to the .355 Hardball cartridge, the case been cut down from 33mm to 25mm and necked down to accommodate a 7.62mm Projectile instead of the usual 9mm. The projectile features a hardened tungsten penetrator tip sharply pointed to aid in penetration, allowing it to pierce through up to level IIIA armor with ease. The projectile body is made of a dense copper alloy, designed to fragment creating grievous wounds on unarmored targets. Propelling a lightweight 92 grain (6 grams) projectile at a brisk 1850 feet per second (564 m/s). Overall, the cartridge has more power and energy compared to 9mm Parumpudicum, giving it a greater recoil impulse than the standard cartridge.

The pistol itself, being based on the Mouletta 99FX uses the same magazines as the original pistol with the same capacity. Just like the stock pistol it is a DA/SA pistol, with the addition of a decocker for carry purposes. The Guillotine features a durable custom frame made of a lightweight Meteora alloy which reduces weight. Though this is offset by the heavier Brigadier style slide and addition of a compensator. The hammer has been skeletonized and all the internals have been replaced with competition grade parts, most notably the trigger which is far lighter and smoother than it's stock counterpart. The magazine release has been made ambidextrous and is extended, the magazine well itself has been beveled to assist in reloading. The grips have been personalized for someone with much smaller hands than normal, which makes it rather uncomfortable to hold. Engraved on the left hand of the slide are the words "La Petite Guillotine".

Instructions:

Controls are identical to the normal 9mm pistol.

## LISA PLASMA BOLTER DMR
*Created by prettyFist*

GZDoom actor name: `HDLisa`

GZDoom ammo actor name: `HDBattery`

Ammo loadout code: `bat`

Default weapon slot: 8

Loadout code: `60d`
* zoom - 16-70, 10x the resulting FOV in degrees
* noscope - Removes the scope
* notech - Analogue scope with no charge indicator

Initially designed on accident after an attempt to create a long distance wireless energy packet transferal obliterated a technician, the device that later became the Lisa was purchased by the Cooper Arms Corporation and distributed en masse as a consumer battery operated model. Firing cycled bolts of hot plasma that disperse through a target, the Lisa is capable of transferring projectile heat directly through armour and clothing but entirely ineffective against shields with any decent strength. Weakened shields will react to the method of plasma creation, causing a chain reaction that bursts the shield as the remaining energy is turned to plasma and caves inward on the target.

Instructions:

Press Fire to blast whatever you're looking at to smithereens.

Hold Altfire to charge the weapon. Up to seven charges can be stored.

Press Firemode to swap between semi and auto mode.

Auto mode will fire off any stored charges immediately. Additional charges can only be generated in Semi mode. 

If a scope is mounted, then hold Zoom and Firemode to adjust scope zoom.

Protip: Ninja Pirate shields are weak enough to be shattered by a single shot. Carefully consider the danger of an immediate explosion caused by shooting an enemy that loves to get in your face. 

##  7MM LOTUS CARBINE
*Created by prettyFist*

GZDoom actor name: `HDLotus`

GZDoom ammo actor name: `SevenMilAmmo`, `SevenMilAmmoRecast`

Ammo loadout codes: `7mm`, `7mr`

Default weapon slot: 8

Loadout code: `l7c`
* sa - 0/1, Single Action Trigger
* scope - 0/1, Boss-Replica Midrange Scope
* barrel - 0/1, Full-Length Rifle Barrel
* suppressor - 0/1, Tsuchinoko Dual-Purpose Suppressor
* bulletdrop - 0-600, amount of compensation for bullet drop
* zoom - 18-50, 10x the resulting FOV in degrees

Built by a privatized group as a modernized replica of an exploded example of medieval gunsmithing found during an archaeological dig, the Lotus is an archaic firearm that does few things but does them well. Despite its size, the weight distribution makes it exceedingly steady for firing full rifle rounds from what some would consider to be the bulkiest end of sidearms. While many decorative markings were not transferred from the original model, the buttstock contains many of them re-added as decoration onto the traction pad.

Instructions:

Controls are identical to the standard revolver.

When reloading, hold Firemode and press Reload to insert recast rounds.

When the scope is mounted, press and hold Zoom and Firemode to adjust scope zoom, or hold Use and Zoom to adjust bullet drop. 

Protip: The Lotus' double action trigger pull is extremely heavy. Pulling the hammer back manually will be necessary for any chance of accuracy in longer-range engagements. 

##  MAULER FRAG CANNON
*Created by Eric*

GZDoom actor name: `HDMauler`

GZDoom ammo actor name: `HDBattery`

Ammo loadout code: `bat`

Default weapon slot: 6

Loadout code: `mlr`
* alt - Start in torpedo mode

After the obvious battlefield success of the BFG9000 prototype, the race began to produce a further miniaturized and streamlined successor for mass production. First to the finish line was the Mauler, a brute force hackjob designed in a single afternoon.  It's a testament to the skill of its engineers that it works at all, inefficient as it is.

While its bombardment capability is far weaker than the full BFG, and all energy production capabilities were discarded, they did hack in a wide-area plasma dispersion function that will annihilate anything in close quarters.

Its poor effective range and high energy demands leave it unpopular among conventional militaries, but rumors have surfaced of a squad of mercenary cultists utilizing it along with a BFG to great effect in retaking and defending dense urban centers.

Instructions:

Press Fire to obliterate whatever is in front of you.

Reloading controls are similar to the Thunder Buster.

Press Altfire or Firemode to cycle between dispersion and torpedo mode.

Protip: To maximize torpedo mode's destructive capabilities, try and launch it into the center of a dense crowd. The projectile inherits your momentum much like the standard BFG, so jumping forward at the time of launching will give it a significant speed boost.

##  ODDBALL PDW
*Created by Combine_Kegan in Decorate, ported to HDest by prettyFist*

GZDoom actor name: `HDOddball`

Default weapon slot: 2

Loadout code: `odd`
* pump - 0/1, Changes the crank for a pump (useful for high latency multiplayer games)

To help combat excessive use of 9mm ammunition, the UAC tasked one of their best R&D employees to create a self sufficient and compact energy weapon for security forces stationed out in remote locations. The scientist was completely stumped on how to accomplish this and his deadline was fast approaching. Certain he was going to lose his job, the scientist left the base to drink his worries away. He had found himself in a bar he had never seen before, and spent the night drinking himself senseless while telling the silent bartender about his current situation. The next day when the scientist had come to, he was shocked to find blueprints for a dynamo powered PDW on his table. The bar was never found again, and others just assumed the scientist was telling tall tales.

The weapon was well recieved by security staff, who affectionally nicknamed it the "Oddball" both for its unorthodox recharging mechanism and the bright eye catching charging handle.

The Oddball fires miniture pellets of energy that are comparable to .22 long rifle ammunition, so what it lacks in velocity or stopping power, it makes up for it in sheer volume of fire. These pellets are capable of causing bleeding wounds, due to the slightly unstable nature of the projectiles, an uncommon feature for an energy weapon.

While it boasts a higher capacity than a standard SMG magazine, recharging the battery in the middle of a fight is far riskier than  simply swapping mags. Along with a time consuming "reloading" period, the weapon's performance suffers slightly as the charge drains, firing slower as the capacitor tries its best to push out the last few remaining shots.

Instructions:

Press Fire to unleash a hail of pellets upon your foes.

Press Reload/Unload to grab the crank. When the crank is grabbed, alternate pressing Fire/Altfire to crank it.

When in pump mode, press Reload/Unload to grab the pump. When the pump is grabbed, hold Fire to pump it forward and release to pump it back.

Protip: What the Oddball lacks in the power of other sidearms it makes up for in both its discouragement and backup weapon potential. It is not recommended to use as a primary weapon.

##  OTIS-5 REVOLVER
*Created by Sledge*

GZDoom actor name: `HDOtisGun`

GZDoom ammo actor names: `HD500SWLightAmmo`, `HD500SWHeavyAmmo`

Ammo loadout codes: `5sw`, `5ld`

Default weapon slot: 2

Loadout code: `ot5`

Barney's personal sidearm. Handcrafted by his good friend Otis Laurey some time after the invasion started. Otis named his own gun Barney, and Barney's gun Otis, so that they'd always be watching over each other. Tragically, just as Barney was arriving with a pizza so that they could watch their favorite movie, Predator, a horde of babuins broke through the window, setting themselves upon Otis.

Dying in Barney's arms, his last words were "My mom's gonna worry when I don't make it home tonight!" Barney has never been the same, since. He set off with Otis's favorite and most prized possession, a Mario Funko Pop, and launched into a personal crusade against Hell. 

It can load two different types of rounds. One normal competition load, and a heavier, slower hand-loaded wildcat cartridge, made for hunting big, bipedal game.

Instructions:

Controls are identical to the normal revolver.

The weapon can hold five shots.

Protip: The Otis is very powerful, especially against living things. Handloads can quickly break shields and armor, but don't hesitate to use a round on anything too close for comfort.

##  PG667 DESIGNATED MARKSMAN RIFLE
*Created by a1337spy*

GZDoom actor name: `HDDMR`

GZDoom ammo actor names: `HD4mMag`

Ammo loadout code: `450`

Default weapon slot: 4

Loadout code: `dmr`
* bulletdrop - 0-1600, amount of compensation for bullet drop
* zoom - 4-12, 10x the resulting FOV in degrees

The LIG Mauer PG667 is a historic collaboration between Volt and LIG Mauer, being the first third party weapon to use the 4.26 UAC Standard round. Due to this, the weapon uses the same 50 round magazines as the venerable ZM66, alongside the UAC Volt Standard Distribution Licensing Protection Protocol. 

However, due to certain trade disagreements between the higher ups of Volt and LIG Mauer, alongside the dual issues of the invasion's sudden onset and the controversial money-saving tactic of buying then throwing out fresh ZM66 rifles to get unbroken magazines, the full implementation of the UAC Volt Standard Distribution Licensing Protection Protocol was canceled, meaning the user only experiences *some* issues with loading unsealed magazines. 

The weapon comes mounted with a LIG Mauer Mambo9 riflescope out of the box, with an integrated LIG Mauer Rumia4 reflex sight as well.

**Warning: LIG Mauer GmbH and the Volt Manufacturing Collective are not liable for any damages caused by reloading with an unsafe magazine. Please refer to your instruction manual provided with your firearm for the full UAC Volt Standard Distribution Licensing Protection Protocol. If you suffer from: Stoppages, Jams, Failures to feed, Failures to eject, Stovepipes, Internal combustion, or other such malfunctions, please contact a licensed LIG Mauer or Volt specialist for further information.**

Instructions:

Press Fire to poke at your enemies from afar.

Reload to swap magazines. You must use fresh magazines.

Press and hold Zoom and Firemode to adjust scope zoom, or hold Use and Zoom to adjust bullet drop.

Protip: The PG667 is much more situational than you may think. Do not treat it as if it were a ZM66.

##  PLASMA PISTOL
*Created by Melodica*

GZDoom actor name: `HDPlasmaPistol`

GZDoom ammo actor name: `HDBattery`

Ammo loadout code: `bat`

Default weapon slot: 2

Loadout code: `plp`

After witness reports of the efficiency of the Thunderbuster's hacked spray mode made their way around, weapons research quickly went into developing weaponry that made full use of that spray mode as their main function. One of the results of these projects was the Plasma Pistol, a small, light plasma weapon capable of both defeating body armor and energy shields, which comes with a charging function to fire larger plasma shots for added shield damage at the cost of more battery usage.

However, after the tragic death of it's inventor after attempting to make the plasma charge more powerful, resulting in the death of all personnel within 5 meters of the weapon, development of the weapon was finalized by the rest of the research team and sent into production, leaving in lower than expected battery efficiency aswell as glaring overheating issues if the user is not careful.

Instructions:

Press Fire to zap your targets with scorching hot plasma.

Hold Fire to charge up the plasma pistol, release to fire. Be careful of the heat buildup though.

While charging, press Altfire to cancel the charge.

Other controls are identical to the stock pistol.

Protip: The plasma pistol performs well against armor and shields. Mind the blast in close quarters.

##  RUBY MICROWAVE LASER
*Created by prettyFist, idea by Yholl*

GZDoom actor name: `RubyMaser`

GZDoom ammo actor name: `HDBattery`

Ammo loadout code: `bat`

Default weapon slot: 6

Loadout code: `mas`
* eject - 0/1, Ejector Model, spits out empty batteries
* alt - 0/1, Start on Wide Beam mode

A microwave laser gun. Roast some people.

Instructions:

Press Fire to cook whatever is in front of you.

Press Altfire to swap between unfocused and focused mode.

Unfocused mode has significantly shorter range but will melt targets alive.

Focused mode does less damage but has much more range than unfocused mode.

When the primary battery is emptied, a backup cell will be loaded.

Press Reload to swap out the main battery.

Press Reload and Use to manually cycle the loaded battery.

Press Alt Reload to swap out the backup battery.

Press Unload to remove the battieres.

##  SAWED OFF SHOTGUN
*Created by Sledge, idea by a1337spy*

GZDoom actor name: `SawedSlayer`

GZDoom ammo actor name: `HDShellAmmo`

Ammo loadout code: `shl`

Default weapon slot: 3

Loadout code: `swd`

It is common practice to (often illegally) take a hacksaw to a perfectly good double barreled shotgun, shave a little bit off the top, and create a short, easily concealable lightweight weapon. These weapons are crude in nature and do not have any of the fancy embellishments (such as a choke, front sight post, stock, or shell holder) seen on their longer iterations. This practice has not changed for hundreds of years, and it will not change in the post invasion paradigm. 

Instructions:

Controls are identical to the default Slayer side-by-side shotgun, sans the shell holder.

You may carry a second Sawed Off Shotgun in your off hand, press Alt Reload to swap between the shotgun in your off hand and your main hand.

Protip: The complete lack of choke results in far less consistent performance compared to the standard Slayer, both in effective range and damage. Use doubleshots more liberally to mitigate low power shells and wider spread patterns. 

##  SCOPED REVOLVER
*Created by Eric*

GZDoom actor name: `HDScopedRevolver`

Ammo loadout code: `355`

Default weapon slot: 2

Loadout code: `srv`
* zoom - 16-70, 10x the resulting FOV in degrees

A modified Buger Blue Deinonychus revolver with a scope and slightly longer barrel. Clean & Simple

Instructions:

Controls are identical to the normal revolver.

Press and hold Zoom and Firemode to adjust scope zoom.

Protip: Unlike the standard revolver, it can fire repeatedly while braced. 

##  SCOPED SLAYER
*Created by Sledge, idea by a1337spy*

GZDoom actor name: `ScopedSlayer`

Ammo loadout code: `shl`

Default weapon slot: 3

Loadout code: `scs`
* lchoke, rchoke - 0-7, 0 skeet, 7 full
* zoom - 5-60, 10x the resulting FOV in degrees

A modified Slayer side-by-side shotgun to have a large hunting scope mounted. Removes iron sights.

Instructions:

Controls are identical to the normal Slayer.

Press and hold Zoom and Firemode to adjust scope zoom.

##  TRO-G ASSAULT Rifle
*Created by Sledge, idea by Combine_Kegan*

GZDoom actor name: `HDTROGRifle`

GZDoom ammo actor names: `HDTrogmag`, `HDRocketAmmo`

Ammo loadout codes: `430`, `rkt`

Default weapon slot: 4

Loadout code: `trg`
* firemode - 0/1, 0 Semiauto, 1 Auto
* glready - 0/1, Hairtrigger GL Loaded

The TRO-G is a business designation for a weapon otherwise known as the Lodyte AR-29. As designed by one Krognar Lodyte, and licensed out to Volt. A military man by trade, and a victim of his own circumstance. One of the earlier and surprisingly reliable adaptations of Volt 4.26 caseless. Too reliable, but not in all of the right ways. The weapon was designed during the Hive City incident, made mostly for combatting the so-called "Tykes" that ran the lower floors of the megacity. Mutated freaks, but not quite demons. 

4.26 sufficed well for a man and for rudimentary armor, but they would occasionally construct and sortie crude armored walkers (FUN-Es). The frangible rounds wouldn't penetrate very well, but the sheer explosive force of a grenade would topple them over, rendering them immediately useless. As such, the grenade launcher was built right into the weapon, rather than as an attachment. 

In a bewildering design decision, the trigger group includes the GL and rifle's triggers right next to each other, under one guard. This proved to be as disastrous under pressure as would be expected, so marines started keeping their GLs unloaded to prevent accidents. Lt. Krognar would later be killed in action during the demon invasion, having abandoned his own rifle for the heavier Liberator, he would be found lagging behind the rest of his squad, overburdened with 7.76, when he might have heard the far off cry of "Hell Knight!" from his team ahead of him. 

A hail of grenades flew over and around him, and he expired cursing out his team for not checking their grenade fire. When pressed for an explanation, SSgt. Eric simply replied "There was a Hell Knight." No further punishment was issued, and the rifle, along with its designer, became a footnote in Volt's history.

Instructions:

Controls are identical to the normal ZM66 rifle.

Press Altfire to fire a rocket grenade. Be extremely careful, as the launcher is on a hair trigger.

Press Alt Reload to insert a rocket grenade into the launcher. 

Press Firemode and Unload to remove a loaded rocket grenade.

Protip: Retain your TRO-G magazines, as in the post 4.26 UAC Standard contract world they are increasingly hard to come by. If you are worried about obliterating your allies or yourself with the launcher, consider keeping it unloaded in between firefights.

##  VERA SQUAD AUTOMATIC WEAPON
*Created by Sledge, idea by Ultra64*

GZDoom actor name: `HDVera`

GZDoom ammo actor name: `HD4mMag`

Ammo loadout code: `450`

Default weapon slot: 4

Loadout code: `m64`
* fast - 0/1, Start at 2100 rpm
* zoom - 16-70, 10x the resulting FOV in degrees

The M764 Squad Automatic Weapon is an experimental 4.26 internal rotary machine gun produced by Tech Industries. Despite their efforts to push this weapon towards the UAC or any other large military force, there were basically no takers. There was no luck with trying to work a deal out with a major arms producer in attempt to widen distribution Toddrick Howarzson, a representative of Volt, had this to say on the matter: "Another 4.26 support weapon? We have the Vulc already. It just works."

One small subset of the UAC was interested in using this weapon. The Offensive Demon Sanitization Trooper division thought it would be a good idea to equip their support units with something lighter and easier to manage than the Vulcanette due to their whole modus operandi being to move quickly while retaking compromised areas. Whether it be for deep ground surveillance, long range reconnaissance operations, or blitzing key locations occupied by the enemy, having a support weapon with more capacity and reliability than the ZM66 but without the weight and stop and go nature of the Vulcanette was very appealing to the ODST division. They are currently the only force out there utilizing the M764 out in the battlefield.

The weapon uses a similar system to the Vulcanette, and therefore has many interchangeable parts. The biggest distinction however is that the M764 internalizes most of the mechanisms, making it less likely to gather up any gunk from an exterior source. It's organized in such a fashion to make fixing parts much faster as well. However, due to this internalization, the weapon requires maintenance more often due to heat building up inside the weapon on heavy usage. The weapon carries significantly less ammunition than the Vulcanette, which is both a benefit and consequence.

There is less ammo to fire, but this makes the weapon able to shoulder on the move. A weapon perfect for providing support fire but still being able to stay with the team without lagging behind. We reached out to a seasoned marine who has carried his own M764 for numerous years to hear how he felt about the weapon. He stated: "I love it. Me and Vera have a couple of life lessons for you!" He then showed us a 47 image slideshow of him and the weapon at various exotic locations, on and off the battlefield. Suffice to say, some soldiers definitely have quite the attachment to this weapon despite how few may be found out in the wild.

Instructions:

Controls are identical to the standard Vulcanette.

Protip: Vera is much more sensitive to heat than the Vulcanette. Use the 2100 RPM mode sparingly, or plan to spend some time repairing your abuse afterwards. 

##  WISAEU PLASMA MAGNUM
*Created by prettyFist*

GZDoom actor name: `HDWiseau`

GZDoom ammo actor name: `HDBattery`

Ammo loadout code: `bat`

Default weapon slot: 2

Loadout code: `wis`
* da - 0/1, Double-Action Trigger
* capacitor - 0/1, Second Capacitor, allows for the weapon to hold a second charge.

Created to hunt vampires by a strange greasy giant, the Wiseau Firearms Compact Lisa Refactor is a modification created by gutting and reconfiguring most of a 0H1-MK "Lisa" Plasma DMR to become a handheld but heavy sidearm. Losing the native gas venting and dispersal in the Lisa's barrel, the Wiseau instead vents all of its hot waste gas outwards at once, causing it to jolt the weapon sharply back despite being an energy based weapon. In addition, the internal safety and automation features being removed to house a more simplistic raw plasma coagulation unit in a smaller space, which cause much stronger energy waste but slowly builds a nearly 90kw burst of energy.

The notable and signature simulated kick combined with the heavy damage of the plasma bolt caused this modification to earn the nickname "Plasma Magnum" and quickly grow in popularity, even among firearm enthusiasts who have sworn off energy weapons. While legal teams at Cooper Arms began to draft DRM to crack down on weapon modifications of the Lisa, one found its way into the hands of the CEO who quickly took a shining to it, later quoting "Haha, what a story!" to the timely halting of the litigation. Cooper Arms would later go on to sell official refactor kits and eventually produce the weapons for market under competition for UAC contract.

Instructions:

Press Fire to reduce the enemy in front of you to its base particles.

Hold Altfire to charge the internal capacitor. If you have a double action trigger installed on the weapon, simply hold Fire to charge the capacitor.

Other controls are identical to the standard pistol.

Protip: The Wisaeu's plasma projectile causes a devastating reaction upon depleting enemy shielding. Combine this with a second weapon to instantly cook Hell Knights, Mancubi, or Arachnotrons.

##  YEYUZE GRENADE LAUNCHER
*Created by a1337spy*

GZDoom actor name: `HDChinaLake`

GZDoom ammo actor name: `HDRocketAmmo`

Ammo loadout code: `rkt`

Default weapon slot: 5

Loadout code: `pgl`

A cheaper alternative to the expensive and heavy magazine fed variant of the Heckler and Mok Urban Artillery series of launchers, The VPF YEYUZE Grenade Launcher (named as such after an incident involving a lake that was close to the weapon's testing grounds) utilizes a similar action to most pump action shotguns, except scaled up for the Short-Range Reduced-Arc Hybrid Rocket Grenade. 

The weapon has seen an unexpected wave of popularity amongst state and non-state actors for both its weight balance to similar launchers and, as one marine put it, "the satisfying action of pumping it." VPF declined to comment on this, but has raked in the profits this weapon has generated and used this to expand their manufacturing facilities.

Instructions:

Controls are identical to the hunter shotgun. All standard grenade launcher functions are present.

Protip: Pressing Use and activating your built in rangefinder will copy the printed range to your YEYUZE's airburst module. This also applies to other launchers with airburst functionality.

##  YUREI HIGH CAPACITY SMG
*Created by Melodica*

GZDoom actor name: `HDYureiSMG`

GZDoom ammo actor name: `HD9mMag50`

Ammo loadout code: `950`

Default weapon slot: 2

Loadout code: `yri`
* firemode - 0/1, 0 Semiauto, 1 Auto
* semi - 0/1, Make it semi-automatic only

A compact submachinegun with high capacity 50 round magazines.

Instructions:

Controls are identical to the SMG.

##  ZM94 "SABREWOLF" ANTI-MATERIEL RIFLE
*Created by Eric, inspired by the ZM69 by Bogus*

GZDoom actor name: `ZM94Rifle`

GZDoom ammo actor names: `HDZM94Mag`, `HD50OMGAmmo`

Ammo loadout codes: `505`, `omg`

Default weapon slot: 8

Loadout code: `z94`
* altreticle - 0/1, whether to use the glowing crosshair on the scope
* frontreticle - 0/1, whether crosshair scales with zoom
* bulletdrop - 0-600, amount of compensation for bullet drop
* zoom - 4-70 (15-40 if frontreticle), 10x the resulting FOV in degrees

A high caliber anti-materiel rifle. Even the Tyrant's toughest won't withstand more than a few rounds of .50 OMG. Requires maintenance when using the burst and automatic firemodes. Compresses to lower bulk when the time is taken to put it away properly. 

Instructions:

Press Fire to punch a golf ball sized hole in what you are aiming at.

Controls for the scope are identical to other scoped weapons.

Press Firemode to cycle between semi, full, and burst mode.

Reload to swap magazines.

Unload to remove the magazine.

Hold Zoom + Unload to attempt to repair the weapon. 

Protip: The delayed recoil of the weapon ensures that the second round of the burst will land exactly on target with the first, provided you aren't moving or firing from a crouching brace. 
